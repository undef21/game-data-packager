Installing Enemy Territory: Quake Wars game data
================================================

Please use game-data-packager to build and install the etqw-data and
etqw-bin packages.

These packages require the commercial game data files from Enemy
Territory: Quake Wars. The official patch for version 1.5
is also required, but can be downloaded automatically.

Typical uses:

    game-data-packager -d ~/Downloads etqw /path/to/search
        Save the generated package to ~/Downloads to install later

    game-data-packager --install etqw /path/to/search
        Install the generated package, do not save it for later

game-data-packager will automatically search various common installation
paths. If you are installing from a CD-ROM or a non-standard location,
add the installation path or CD-ROM mount point to the command line.

Add the path to ETQW-client-1.5-full.x86.run, ETQW-client-1.0-1.5-update.exe
or ETQW-client-1.4-1.5-update.x86.run to the game-data-packager command line
if you already have them available locally, to avoid re-downloading them.

For more details please see the output of "game-data-packager etqw --help"
or the game-data-packager(6) man page.
