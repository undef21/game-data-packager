#!/usr/bin/make -f
# GNU copyright 1997 to 1999 by Joey Hess.

#export DH_VERBOSE=1
export DH_OPTIONS

include /usr/share/dpkg/pkg-info.mk

%:
	mkdir -p $(HOME)
	dh $@ --with bash-completion --buildsystem=meson

# Redirect Inkscape log files, etc. to a temporary directory
export HOME = $(CURDIR)/debian/temp-home
export XDG_CACHE_HOME = $(CURDIR)/debian/temp-home/.cache
export XDG_CONFIG_HOME = $(CURDIR)/debian/temp-home/.config
export XDG_DATA_HOME = $(CURDIR)/debian/temp-home/.local/share
# Disable tests that are not appropriate at build-time
export DEB_BUILD_TIME_TESTS = 1

override_dh_auto_configure:
	dh_auto_configure \
		-- \
		--bindir=games \
		--libdir=lib \
		-Dgamedatadir=share/games \
		$(NULL)

override_dh_auto_build:
	dbus-run-session -- dh_auto_build \
		-- \
		$(NULL)

execute_after_dh_auto_clean:
	@if [ "`echo *.deb`" != "*.deb" ]; then \
		echo; \
		echo "*************************************************"; \
		echo "Cannot create source package with these here:"; \
		ls *.deb; \
		echo "*************************************************"; \
		echo; \
		exit 1; \
	fi
	debian/copyright.py

execute_after_dh_install:
	if dpkg-vendor --derives-from Ubuntu; then \
		install -d debian/game-data-packager/usr/share/games/game-data-packager; \
		touch debian/game-data-packager/usr/share/games/game-data-packager/is-ubuntu-derived; \
	fi
	dh_apparmor -pgame-data-packager-runtime --profile-name=usr.lib.unreal
	dh_apparmor -pquake4 --profile-name=usr.lib.quake4.quake4.x86
	dh_apparmor -pquake4-server --profile-name=usr.lib.quake4.q4ded.x86

override_dh_installdocs:
	dh_installdocs -XTODO

override_dh_installsystemd:
	dh_installsystemd -pquake4-server --no-enable --no-start
	dh_installsystemd -petqw-server --no-enable --no-start
	dh_installsystemd --remaining-packages

execute_after_dh_python3:
	dh_python3 -pquake-server /usr/share/games/quake
	dh_python3 -pquake2-server /usr/share/games/quake2
	dh_python3 -pquake3-server /usr/share/games/quake3
	dh_python3 -pquake4-server /usr/share/games/quake4
	dh_python3 -petqw-server /usr/lib/etqw
