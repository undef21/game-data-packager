# SPDX-FileCopyrightText: 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

option(
  'bash_completion_dir',
  type : 'string',
  description : 'install bash completion script in this directory',
  value : '',
)
option(
  'distro',
  type : 'string',
  description : 'Linux distribution family [default: automatic]',
  value : '',
)
option(
  'format',
  type : 'string',
  description : 'Default format for output packages [default: automatic]',
  value : '',
)
option(
  'gamedatadir',
  type : 'string',
  description : 'Base directory for game data, e.g. share/games [default: same as datadir]',
  value : '',
)
option(
  'program_prefix',
  type : 'string',
  description : 'Prepend string to program names',
)
