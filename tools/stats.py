#!/usr/bin/python3
# encoding=utf-8
#
# Copyright © 2015 Alexandre Detiste <alexandre@detiste.be>
# SPDX-License-Identifier: GPL-2.0-or-later

from game_data_packager.game import (load_games)
from game_data_packager.packaging.deb import (DebPackaging)


class Package:
    game: str
    year: int
    type: str
    fanmade_: bool
    package: str
    disks: int = 1
    engine: str = ''
    size_min: int
    size_max: int

    def fanmade(self) -> str:
        return {True: 'Y'}.get(game.fanmade, 'N')

    def order(self) -> int:
        return {
            'demo': 1,
            'full': 2,
            'expansion': 3,
            'doc': 4
        }.get(self.type, 5)


games: list[Package] = []
packaging = DebPackaging()

for name, game in load_games().items():
    game.load_file_data()
    for package in game.packages.values():
        if package.rip_cd:
            continue
        p = Package()
        p.game = name
        p.package = package.name
        p.type = package.type

        engine = package.engine or game.engine or ''
        if type(engine) is dict:
            engine = packaging.substitute(engine, name)
        assert type(engine) is str
        p.engine = engine.split(' ')[0]

        copyright = package.copyright or game.copyright
        assert type(copyright) is str
        p.year = int(copyright.split()[1][0:4])

        p.size_min, p.size_max = game.size(package)
        p.fanmade_ = game.fanmade
        games.append(p)

games = sorted(games,
               key=lambda k: (k.game, k.order(), k.package))

print('GAME;YEAR;TYPE;FANMADE;PACKAGE;DISKS;ENGINE;SIZE_MIN;SIZE_MAX')
for g in games:
    print('%s;%d;%s;%s;%s;%d;%s;%d;%d' %
          (g.game, g.year, g.type, g.fanmade(), g.package,
           g.disks, g.engine, g.size_min, g.size_max))
