#!/bin/sh
# Copyright 2023 Alexandre Detiste
# SPDX-License-Identifier: FSFAP

# minimal pre-commit hook to install into .git/hooks/pre-commit

set -e
set -u

CHANGES=$(git diff --name-only --cached --diff-filter=ACMR -- '***.py')

if [ -z "$CHANGES" ]
then
    exit 0
fi

pycodestyle $CHANGES
pyflakes3 $CHANGES

# https://github.com/fsfe/reuse-tool/issues/193
for change in $CHANGES
do
    if ! grep -q "SPDX-License-Identifier" "$change"
    then
        echo "missing SPDX info: in $change"
        exit 1
    fi
done
