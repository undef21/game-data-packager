#!/bin/bash
# Copyright 2021 Alexandre Detiste
# SPDX-License-Identifier: GPL-2.0-only

set -e

cat data/*.yaml | grep url: | awk '{ print $2}' | sort -u > a
curl -sS https://wiki.scummvm.org/index.php?title=Where_to_get_the_games | grep -o -e https://www.gog.com/game/[a-z0-9_]* | sort -u | cut -d '/' -f 5 > b
diff -u a b | grep ^+
