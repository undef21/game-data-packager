# Copyright 2022 Simon McVittie
# SPDX-License-Identifier: FSFAP

configure_file(
  copy : true,
  input : meson.project_source_root() / 'icons' / 'quake3-tango.png',
  output : 'quake3.png',
  install_dir : get_option('datadir') / 'icons' / 'hicolor' / '256x256' / 'apps',
)

configure_file(
  copy : true,
  input : meson.project_source_root() / 'icons' / 'quake3-teamarena-tango.png',
  output : 'quake3-team-arena.png',
  install_dir : get_option('datadir') / 'icons' / 'hicolor' / '256x256' / 'apps',
)

foreach icon : quake_icons
  custom_target(
    icon['name'] + '.png',
    build_by_default : true,
    input : icon['svg'],
    output : icon['name'] + '.png',
    command : [
      'inkscape',
      '--export-area=0:0:256:256',
      '--export-width=256',
      '--export-height=256',
      '--export-id=layer-' + icon['game'] + '-256',
      '--export-id-only',
      '--export-filename=@OUTPUT@',
      '@INPUT@',
    ],
    install : true,
    install_dir : get_option('datadir') / 'icons' / 'hicolor' / '256x256' / 'apps',
  )
endforeach
