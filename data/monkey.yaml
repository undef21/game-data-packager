---
franchise: Monkey Island
longname: The Secret of Monkey Island
copyright: "\u00a9 1990 Lucasfilm Games"
plugin: scummvm_common
gameid: "scumm:monkey"
wiki: The_Secret_of_Monkey_Island
wikipedia: https://en.wikipedia.org/wiki/The_Secret_of_Monkey_Island

help_text: |
   The Special Edition as sold on Steam & GOG.com is not supported:

   http://forums.scummvm.org/viewtopic.php?t=7671

packages:
  secret-of-monkey-island-en-data:
    provides: secret-of-monkey-island-data
    install:
    - assets english
    optional:
    - midi roms
    - some flac rip

  secret-of-monkey-island-floppy-en-data:
    better_version: secret-of-monkey-island-en-data
    provides: secret-of-monkey-island-data
    install:
    - assets floppy english

  secret-of-monkey-island-de-data:
    provides: secret-of-monkey-island-data
    lang: de
    install:
    - assets german
    optional:
    - midi roms
    - some flac rip

  secret-of-monkey-island-floppy-de-data:
    version: "1.1"
    better_version: secret-of-monkey-island-de-data
    lang: de
    provides: secret-of-monkey-island-data
    install:
    - assets floppy german

  secret-of-monkey-island-fr-data:
    provides: secret-of-monkey-island-data
    lang: fr
    install:
    - assets french
    optional:
    - midi roms
    - some flac rip

  secret-of-monkey-island-it-data:
    provides: secret-of-monkey-island-data
    lang: it
    install:
    - assets italian
    optional:
    - midi roms
    - some flac rip

groups:
  # PC VGA versions
  assets english:
    group_members: |
      8955      2d1e891fe52df707c30185e52c50cd92 monkey.000
      4779713   d074150de681d49ff350e8ad60ee4abd monkey.001

  assets german:
    group_members: |
      8955      305d3dd57c96c65b017bc70c8c7cfb5e monkey.000?de
      4790965   9a70eb103fbd71d24a5d606d27fdc3b2 monkey.001?de

  assets floppy english:
    group_members: |
      8357      15e03ffbfeddb9c2aebc13dcb2a4a8f4 000.lfl?en_floppy
      2264      280f20d081cc7701c38dfdc8b701c58c 901.lfl?en_floppy
      3335      13a9297cca1b33c4ef9c24ed1dacc64d 902.lfl?en_floppy
      2019      0c60481ff858c0e8ab136adab5fa5bdc 903.lfl?en_floppy
      4368      6c99003057f3b84d9fedf7a88dba813b 904.lfl?en_floppy
      1099338   0c5f3ffb058fab70f1fad885bdcffe4a disk01.lec?en_floppy
      1114187   e68987c6d06e48ccc91f0161d3cf7631 disk02.lec?en_floppy
      1181719   cee6499a8d29a92c370bb293c5e0c4ee disk03.lec?en_floppy
      1046757   8de9e90998a81c3c2b5aabd11d20aa87 disk04.lec?en_floppy

  assets floppy german:
    group_members: |
      8357      d0b531227a27c6662018d2bd05aac52a 000.lfl?de_floppy
      2572      0d01659586aa770608acc8138daae8cc 901.lfl?de_floppy
      4023      838b3d8ed6fa615ae561273ae29efafd 902.lfl?de_floppy
      2572      6c72dd66cccae6308f962075c314ee16 903.lfl?de_floppy
      4782      87edc5c6d4eb64361df02072e08732ce 904.lfl?de_floppy
      1101583   f4762f06c3de37d852ff16f093a06406 disk01.lec?de_floppy
      1118422   b74e2034cac3bb3203ee28d4a7d18d4e disk02.lec?de_floppy
      1176753   b29b164bbd996e18c3387438e5d7a734 disk03.lec?de_floppy
      1048787   a0c2a3b5dadaf6e09ecaa819752aa4a3 disk04.lec?de_floppy

  assets french:
    group_members: |
      8955      aa8a0cb65f3afbbe2c14c3f9f92775a3 monkey.000?fr
      4794979   5b6d35c93e5eae9a2b8911a1bbf46990 monkey.001?fr

  assets italian:
    group_members: |
      8955      da6269b18fcb08189c0aa9c95533cce2 monkey.000?it
      4786822   11069aae43a6db6b3a6097d8eafaacc1 monkey.001?it

  # these are shared by many games
  midi roms:
    group_members: |
      65536     9513fec4f09a7d327748340ce3a2a59b mt32_control.rom
      524288    89e42e386e82e0cacb4a2704a03706ca mt32_pcm.rom

  # not reproducible
  some flac rip:
    group_members: |
      9535864   d171e0c42652c625559b0da3fc4550f5 track1.fla
      7472775   53390bfbe92b9aaf8a74e318a7f2d237 track2.fla
      7289043   92e49914f475062ec18d31fb8b182244 track3.fla
      11948882  94ded1727909330a29f192306ee726a6 track4.fla
      11201475  efd1b21e5ce9657fbdb656acc09899ec track5.fla
      787598    b039d8d287011cea7c3fe6eb553442d9 track6.fla
      5564480   ca0229a5293c76651c0c289f25e273bf track7.fla
      11411502  6f83326afce276486eb82cdd7962969a track8.fla
      12166345  224bad30ac9878d633ae5c8781477dfd track9.fla
      7163557   4e1000f52b082f5e312cf5bbc811ba3a track10.fla
      12450379  c071b2807b77b8375c83aaf52b28510d track11.fla
      9694598   72e51a24b6f19c33f6c14d8dfc15f77d track12.fla
      1271485   37360951d1bd9c5f37934250ff63510c track13.fla
      11325790  fd76cc9d790753b351ce8b6dde09853b track14.fla
      11830642  a8f63ef699028952250abb5ed666b6c8 track15.fla
      17328574  20b33da7e0a063c7101f176cc27b2d09 track16.fla
      13090753  584d923448c34173657650850be9657f track17.fla
      13859734  eeef0136323a14300cc38eb844ea9717 track18.fla
      1313844   5c1ee4340157bc19e418529659930b11 track19.fla
      1147254   e4cfd5e351661ab9cf4f87788c6b4396 track20.fla
      12240786  7a59c27e9b587df5603982e9c077b2a2 track21.fla
      11603219  374e6ac705ea9f816d3c4bbab02036ac track22.fla
      6879737   055c9b50479e580a3bc469babda9fb1f track23.fla
      9681311   021535a118b56e222b3497be79a73427 track24.fla

sha1sums: |
  65e1ba6557eb839a530fb17c97adbef226c17c7b 000.lfl?en_floppy
  bb4a3b8f2af0c919e2d7b3b01ae3b8837ffd8a6f 901.lfl?en_floppy
  c72b60b4f58603ef5651d66e708fc5b8fd776f5a 902.lfl?en_floppy
  8ad3b1901bac27c7d1bf61e55bb43de3d1ddedc1 903.lfl?en_floppy
  0685377f67a289e70c0fd22435ee6183aa659b74 904.lfl?en_floppy
  7edae74fe31cbaea137fcc64c1aa81457e661977 disk01.lec?en_floppy
  61a9619fdd261d6e04a8de4130971bdde1ec2d3e disk02.lec?en_floppy
  dee3329632adf918941012dd41fd60b040a2f747 disk03.lec?en_floppy
  d818a83e41dd8f84542a3b263c9fb6aa56daf1d2 disk04.lec?en_floppy
  928c2cb962759e69af2d1fdafb9722b378471a43 000.lfl?de_floppy
  a1275e33074eff49c08f1a61ec9d7efed37e8c9d 901.lfl?de_floppy
  39b5002abaf0072468cb3dea1c20e6142569d3d0 902.lfl?de_floppy
  5c1d9f65166e9b76c908c1c8dcd4b063fbc384d6 903.lfl?de_floppy
  7fee10fcf5aa5499f880947dddb9f9e939b719cc 904.lfl?de_floppy
  9d363b1e842714567d1926852729a1ef1c459d68 disk01.lec?de_floppy
  4aa39091f635e904a50fffb05a823587e2c31bd2 disk02.lec?de_floppy
  5e5be9e868540e91fd6dc77fba782da9cea6aace disk03.lec?de_floppy
  780bd6c83beb242e010fb5873bfe49c1841c71ed disk04.lec?de_floppy
  3b523e5b3924823d847455ece204be13aee876e0 monkey.000
  10c8d6ddd3dd68ad78b6bfe33e67d8ce9d1e7ecd monkey.000?de
  cf0c6ae473d8ddcd1dd62c4abc77f0bfbd5217c5 monkey.000?fr
  f4dd3c0793c5e250aaf9a937c5762f3d38433bad monkey.000?it
  f17c92600894bf9b2e840e7adae03ef6966b5ca2 monkey.001
  5005ffb054d80d9af652ee58f8fae48d76a5554e monkey.001?de
  b6a761fc80108a3c85e0b9edbf0f90caf79ba2b3 monkey.001?fr
  da5eca2c4fc3fc4d3cd22dd77e1d50db128f1c5e monkey.001?it
  7b8c2a5ddb42fd0732e2f22b3340dcf5360edf92 mt32_control.rom
  f6b1eebc4b2d200ec6d3d21d51325d5b48c60252 mt32_pcm.rom

sha256sums: |
  a31b3955c1c8be2ee36efffd97d7641441cd8717db58539ee88566c5b50e442c 000.lfl?en_floppy
  0d341d1dfec0399083ef2c274cbb3464e1f1fba16090450f48f9551aabe3a023 901.lfl?en_floppy
  2b76090dc87fcae88fa1a850adac84e3314713fda193f6c6312822593e285608 902.lfl?en_floppy
  6b5426627b7e7704779ecf40116c66a6563f036776611d4bc5cd033e985c221c 903.lfl?en_floppy
  9c90c965b6f110a057cc7524e044984123b3940dcba81c6487abe7388b29a415 904.lfl?en_floppy
  bfa0d618af787a251dc19ef657795adc54a15dd08599082df16c9052a6711b06 disk01.lec?en_floppy
  311f4c3c149a667afaaf11797f7a7eff88019690a8d1d0746043ea45ea455515 disk02.lec?en_floppy
  200cdd63c19fbbc967c0fffa3d5838127ae38f12fe479adc778c7ae6703b583f disk03.lec?en_floppy
  05be108877a3702a7775ece78d46f4fbd8447b383f77d5599179aa2e1b9d67cb disk04.lec?en_floppy
  c2d8c664028d326385de016b9d781fc0a33917a1de3032b5f2fce1191baa8522 000.lfl?de_floppy
  cf05602bdfea70f0c28f3755d91379b7f9ac5e54bf6a0b1a5451158cf02e3888 901.lfl?de_floppy
  2bdbb2238f0ee473f1903693337569b18fc1e216f3286b23a7336a38536ee3ce 902.lfl?de_floppy
  8746efe58492a13209eb25cfe840cbee4db42ec975d4efcb2bfaba4cb5dfb8a0 903.lfl?de_floppy
  89999ddc9b547ffae381ae13760f36eb5ca1f149dcf02bf4c0329d6f39333ff5 904.lfl?de_floppy
  8cbe3f6f61c58d4a60dd4798e8ff89ec3c585b86ecd4250f817c5b044bd508f2 disk01.lec?de_floppy
  3d8b5a4b182e2ad6c0dd2c8ef2d43dd16ca1dbd860506a096bcfc45f81f89951 disk02.lec?de_floppy
  5ca1367696207fe60347905ba96d2b729fdb8a97ac701f54fad5658282c07f50 disk03.lec?de_floppy
  727f8c8fdbd0595eba91870915db457cd3c2b18407174bdcda000af4eac4626f disk04.lec?de_floppy
...
