---
longname: Descent 3
franchise: Descent
copyright: "\u00a9 2000 Loki Software"
genre: Flight simulator
wikipedia: https://en.wikipedia.org/wiki/Descent_3
binary_executables: i386
plugin: loki_shim

packages:
  descent3:
    empty: true
    architecture: i386
    version: "1.4.0b"
    depends:
      - game-data-packager-runtime (>= 77~)
      - descent3-bin (>= 1.4.0b)
      - descent3-data (>= 1.4.0b)
    symlinks:
      $bindir/descent3:
        $assets/game-data-packager-runtime/gdp-launcher
      $prefix/share/applications/descent3.desktop:
        $assets/game-data-packager-runtime/descent3.desktop
      $prefix/share/pixmaps/descent3.xpm:
        $assets/descent3/icon.xpm
      $prefix/lib/descent3/demo:
        $assets/descent3/demo
      $prefix/lib/descent3/missions:
        $assets/descent3/missions
      $prefix/lib/descent3/movies:
        $assets/descent3/movies
      $prefix/lib/descent3/netgames:
        $assets/descent3/netgames
      $prefix/lib/descent3/online:
        $assets/descent3/online
      $prefix/lib/descent3/d3.hog:
        $assets/descent3/d3.hog
      $prefix/lib/descent3/d3-linux.hog:
        $assets/descent3/d3-linux.hog
      $prefix/lib/descent3/extra.hog:
        $assets/descent3/extra.hog
      $prefix/lib/descent3/extra13.hog:
        $assets/descent3/extra13.hog
      $prefix/lib/descent3/ppics.hog:
        $assets/descent3/ppics.hog
      $prefix/lib/descent3/merc.hog:
        $assets/descent3/merc.hog

  descent3-bin:
    data_type: binaries
    install_to: usr/lib/descent3
    architecture: i386
    version: "1.4.0b"
    install:
      - binaries on cd
      - patched binaries
      - lokishim.zip
    build_depends:
      - deb: gcc-multilib
        rpm: gcc
      - rpm: glibc-devel.i686
    depends:
      - libc.so.6
      - libgl1
      - libSDL-1.2.so.0
      - libsmpeg-0.4.so.0

  descent3-data:
    version: "1.4.0b"
    disks: 2
    install:
      - assets on cd
      - assets in data.tar.gz
      - patched assets
    doc:
      - documentation on cd
      - README.txt?140b

  descent3-mercenary-data:
    expansion_for: descent3-data
    longname: "Descent 3: Mercenary"
    install_to: $assets/descent3
    version: "1.4.0b"
    install:
    - mercenary assets
    doc:
    - README.mercenary

files:
  lokishim.zip:
    download: https://github.com/twolife/lokishim/archive/b1019abf8b4a47feae4e0f6b0d098a67655d08d2.zip
    install_to: $assets/descent3

  descent3-1.4.0a-x86.run:
    download:
      lokigames-mirrors:
        path: updates/descent3
    unpack:
      format: tar.gz
      skip: 5847
    provides:
      - files in 1.4.0a patch

  descent3-1.4.0b-x86.run:
    download:
      lokigames-mirrors:
        path: updates/descent3
    unpack:
      format: tar.gz
      skip: 5961
    provides:
      - files in 1.4.0b patch

  README.mercenary:
    download: https://mirrors.dotsrc.org/lokigames/updates/descent3/mercenary-1.0.run.txt

  d3merc.pkg:
    unpack:
      format: d3pkg
    provides:
      - mercenary assets

  data.tar.gz:
    unpack:
      format: tar.gz
    provides:
      - assets in data.tar.gz

  shared.tar.gz:
    unpack:
      format: tar.gz
    provides:
      - superseded assets in shared.tar.gz

  descent3?140b:
    executable: true
    install_as: descent3

  descent3.dynamic?140b:
    executable: true
    install_as: descent3.dynamic

  bin/x86/glibc-2.1/nettest:
    executable: true
    install_as: nettest

  descent3.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - bin/x86/glibc-2.1/descent3
    provides:
      - descent3?140a

  descent3.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - descent3?140a
    provides:
      - descent3?140b

  descent3.dynamic.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - descent3?140a
    provides:
      - descent3.dynamic?140b

  d3-linux.hog.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - d3-linux.hog
    provides:
      - d3-linux.hog?140a
  d3-linux.hog.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - d3-linux.hog?140a
    provides:
      - d3-linux.hog?140b

  netgames/Anarchy.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/Anarchy.d3m
    provides:
      - netgames/Anarchy.d3m?140a
  netgames/Anarchy.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/Anarchy.d3m?140a
    provides:
      - netgames/Anarchy.d3m?140b

  netgames/co-op.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/co-op.d3m
    provides:
      - netgames/co-op.d3m?140a
  netgames/co-op.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/co-op.d3m?140a
    provides:
      - netgames/co-op.d3m?140b

  netgames/ctf.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/ctf.d3m
    provides:
      - netgames/ctf.d3m?140a
  netgames/ctf.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/ctf.d3m?140a
    provides:
      - netgames/ctf.d3m?140b

  netgames/entropy.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/entropy.d3m
    provides:
      - netgames/entropy.d3m?140a
  netgames/entropy.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/entropy.d3m?140a
    provides:
      - netgames/entropy.d3m?140b

  netgames/hoard.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/hoard.d3m
    provides:
      - netgames/hoard.d3m?140a
  netgames/hoard.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/hoard.d3m?140a
    provides:
      - netgames/hoard.d3m?140b

  netgames/hyperanarchy.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/hyperanarchy.d3m
    provides:
      - netgames/hyperanarchy.d3m?140a
  netgames/hyperanarchy.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/hyperanarchy.d3m?140a
    provides:
      - netgames/hyperanarchy.d3m?140b

  netgames/monsterball.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/monsterball.d3m
    provides:
      - netgames/monsterball.d3m?140a
  netgames/monsterball.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/monsterball.d3m?140a
    provides:
      - netgames/monsterball.d3m?140b

  netgames/robo-anarchy.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/robo-anarchy.d3m
    provides:
      - netgames/robo-anarchy.d3m?140a
  netgames/robo-anarchy.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/robo-anarchy.d3m?140a
    provides:
      - netgames/robo-anarchy.d3m?140b

  netgames/tanarchy.d3m.0?140a:
    unpack:
      format: xdelta
      other_parts:
        - netgames/tanarchy.d3m
    provides:
      - netgames/tanarchy.d3m?140a
  netgames/tanarchy.d3m.0?140b:
    unpack:
      format: xdelta
      other_parts:
        - netgames/tanarchy.d3m?140a
    provides:
      - netgames/tanarchy.d3m?140b

  README.txt?140b:
    install_as: README-1.4.0b

groups:
  misc:
    group_members: |
      330722370 f5d331b3666768b0254a31840d9ae642 d3merc.pkg
      8065846   2aafce69125ca67a4187d0cce8c37d60 data.tar.gz
      4863053   b62e6873c45acea7ab9451f820373509 shared.tar.gz
      2736956   252e242e57e15a9afab7d6fd69f562ae descent3-1.4.0a-x86.run
      4504631   4cfd46f594fa40b2afd575842a197ba1 descent3-1.4.0b-x86.run
      3541      0573964d3eb7920ef423b157c0b6ee7f lokishim.zip

  documentation on cd:
    group_members: |
      7839      8519feb5128ec0548de2a90673fa245a FAQ.txt
      7172      9ddc915914afd1d9ef2b525346d21a26 README

  files in 1.4.0a patch:
    group_members: |
      901       cab006f941563e977f0d2e3667beac94 d3-linux.hog.0?140a
      873091    21ca9366de18ae3e266b77d0061d1e52 descent3.0?140a
      107562    a2a2bf2e71a71cddb07e96c973f422fe netgames/Anarchy.d3m.0?140a
      106400    a4138d9998461618f0da7830cbe88b5d netgames/co-op.d3m.0?140a
      113778    6a5006f0fb2bfb96ef072ceeb994efe3 netgames/ctf.d3m.0?140a
      112806    c09163168317a95c2da050113ad3be57 netgames/entropy.d3m.0?140a
      110648    36259186fa7fbaa0f2699d5c8acb7566 netgames/hoard.d3m.0?140a
      109958    2407913d6b56ff144ef9f548891a2bb4 netgames/hyperanarchy.d3m.0?140a
      110941    b6465950964a42b6b524d12408c7e82f netgames/monsterball.d3m.0?140a
      107707    bed0ce1975fce441d16a2539e9f3cc84 netgames/robo-anarchy.d3m.0?140a
      107741    328a5cda0c481b79c8073c0db9a4ebf0 netgames/tanarchy.d3m.0?140a

  files in 1.4.0b patch:
    group_members: |
      5832      ab58802d426aeb451a2bfe9a6c593a01 README.txt?140b
      842       1f4cd41233f9a25550027de4467470c0 d3-linux.hog.0?140b
      837823    40926a2cfd77f4023a368d321a460326 descent3.0?140b
      689825    ba4779c2326e711236c2d4ae63f93ae5 descent3.dynamic.0?140b
      306179    66fa04002124316bd9ae2b373e2364a6 netgames/Anarchy.d3m.0?140b
      311486    37433456e57107b8f42c79d5c5fb5621 netgames/co-op.d3m.0?140b
      286126    255b4f176493a3a710f04b721865908c netgames/ctf.d3m.0?140b
      284399    66c17bcd5bf395f5d132c60eb00bb8af netgames/entropy.d3m.0?140b
      280541    7383b80d111cf44ec2003c13bab7c185 netgames/hoard.d3m.0?140b
      322894    a6338a19328bf02c60b3931e2ae025a4 netgames/hyperanarchy.d3m.0?140b
      284563    882bead66ab2ec7f950399b91b0b41ed netgames/monsterball.d3m.0?140b
      306218    745cead757e8de11a47bae06bb641899 netgames/robo-anarchy.d3m.0?140b
      275527    a08d440f81b959bac4c6f6f05882eef1 netgames/tanarchy.d3m.0?140b

  patched binaries:
    group_members: |
      3427284   3dbd6f65c8bcab3fb0ab959c3bf532c0 descent3?140b
      2890856   647c8047b9f3c05aabb123896334d9c2 descent3.dynamic?140b

  patched assets:
    group_members: |
      11749941  c412fd47a35c13fa61fc2e8c8c6b5649 d3-linux.hog?140b
      1484888   f93f0a6cf3cc494430995469bf35984d netgames/Anarchy.d3m?140b
      1468430   41eda3a8db3308121af66eed5c592849 netgames/co-op.d3m?140b
      1410852   54557dcbb46c8372f027da2445454ed8 netgames/ctf.d3m?140b
      1402245   d0446114226f2a29845c35135c84b57c netgames/entropy.d3m?140b
      1378091   7c40ccb4bd4569ef060a103b5e4f99d0 netgames/hoard.d3m?140b
      1528524   0797658a70a96e729165b10c6bca9f30 netgames/hyperanarchy.d3m?140b
      1398308   883ce310889903d16f51c77a3631762a netgames/monsterball.d3m?140b
      1484908   af103a816c6b3b13d645f53e4a5cef70 netgames/robo-anarchy.d3m?140b
      1348889   8ff3a92d1aa263934ca4f1880c2e715b netgames/tanarchy.d3m?140b

  superseded files from patch 1.4.0a:
    group_members: |
      11749941  2a076de7225acd8cf595cbfe9155704d d3-linux.hog?140a
      3205176   d49d87ccf0865b30479025cdff65461b descent3?140a
      336004    756968dad55f52d15b9554959d5f9e19 netgames/Anarchy.d3m?140a
      327044    a3bf976871b0394ad532ebb051959a9d netgames/co-op.d3m?140a
      361124    895e2b7d9140d9c646ccab26eb0b078e netgames/ctf.d3m?140a
      355736    69dc23522899536bd808b1622301a904 netgames/entropy.d3m?140a
      346592    4652ca64e569994af9df1ab5d4d1bf70 netgames/hoard.d3m?140a
      343940    cbe50fb6cb347c00198b8ba1cd66198e netgames/hyperanarchy.d3m?140a
      354788    e75e3eda6372b1043d5d71bfbbba7f7e netgames/monsterball.d3m?140a
      336004    9936105f44c9b9a1dc5cabce4fd76b7c netgames/robo-anarchy.d3m?140a
      336132    1ad9c962094917aea9f2159968ccf562 netgames/tanarchy.d3m?140a

  binaries on cd:
    group_members: |
      41524     f4ec372c8e1bfc74665414fd9c7f48b7 bin/x86/glibc-2.1/nettest

  superseded binaries on cd:
    group_members: |
      3226500   62bf8884ac14cc1b16377479f2fdbfb7 bin/x86/glibc-2.1/descent3

  superseded assets in shared.tar.gz:
    group_members: |
      11749905  a4cfff7ba298023c1114e526241ea295 d3-linux.hog
      409440    bcd9402d548085a50a100b3a2f1f5384 netgames/Anarchy.d3m
      400448    743eaf33da3022678e033d8cbead0104 netgames/co-op.d3m
      434560    f5af9b5335fcb6d8984e8fedecf1b0b5 netgames/ctf.d3m
      429332    af2b0ca6c75be7bdd277eb0c57a17c06 netgames/entropy.d3m
      420220    bce3dc6b7a7cb0442ef65f5598db80f6 netgames/hoard.d3m
      417376    70074c9bbaf04860c9c8f8ff8875a0cd netgames/hyperanarchy.d3m
      428224    ba69b74b42b988605f78a7895c90cb15 netgames/monsterball.d3m
      409472    5670dd6f0cb4b00b7fc63e710bca6f85 netgames/robo-anarchy.d3m
      409568    a99d39740a2d7e78ad1f34ed0057c43b netgames/tanarchy.d3m

  assets in data.tar.gz:
    group_members: |
      290       7b24f1aca5cf1754faf4b23431c2d959 Dedicated.cfg
      5087119   1ad34c88b28d2b91d0bda9b3d38ffd26 demo/Secret2.dem
      4         cb492b7df9b5c170d7c87527940eff3b demo/empty
      476566    f85aeb616f6526abc3b69ede85b7635d extra.hog
      278329    abd02761ac04fe9677c661d62dadc446 extra13.hog
      201246    173f51e13fdb09ee19c0d4545b1492eb imd.bmp
      722       bdd253dc5767d03c698c276a6b86de23 missions/bedlam.mn3
      510       e7d8187b94f94cfac6839d6b88d0147f missions/dementia.mn3
      579       dbf2ef182886159790ca7bb38ab6742e missions/frenzy.mn3
      908       b8e0e525343ce15d14024da0ca605c1a missions/fury.mn3
      5059244   ead22d09da4c4656b86cb9e539c9f545 missions/training.mn3
      74        0af27229afd30d84f12d55619d3c5d80 online/Direct TCP~IP.d3c
      74        0af27229afd30d84f12d55619d3c5d80 online/Parallax Online.d3c
      5611278   12da2d2b18e804a6b22b211e0ac2a2f7 ppics.hog

  assets on cd:
    group_members: |
      194030423 5d128368aee24846d4eedbe931db5649 d3.hog
      6966      2935b1c99a72d13b2762a5cae8c91dca icon.bmp
      8930      24eaf0c9559f81ced30ace1c4138a1e2 icon.xpm
      77937559  ed02f9a93e4be4286f73de3c773fb541 missions/d3.mn3
      215599121 c0ec8a44b8ec25d7dea8a91654877f4f missions/d3_2.mn3
      4235302   701fae7b8707cd6565fe39e8264776c4 missions/d3voice1.hog
      5533265   c78ee97c739005b09e637578a20b1be9 missions/d3voice2.hog
      160231838 b5bb396aee367f55332aefbaff06ea41 movies/end.mve
      148738382 cb133a8eadd543d322cfab14b78bde08 movies/intro.mve
      155417956 7f5fb6d0c590c1c7bc92c538e774499f movies/level1.mve
      32122472  aec189ec8949862e6205fa72f63035de movies/level5.mve

  mercenary assets:
    group_members: |
      212406907 6074b2472abd8300ea84d5cd94a44909 merc.hog
      9537069   58b8ae75dc2aca3ac6ab5edee428327a missions/CHAOS.MN3
      21360344  d94abae6d41e20697dde0f6668a4b636 missions/HAVOC.MN3
      2511539   6318d145ce5a3a028b0dbf8793d109f0 missions/MAYHEM.MN3
      2539404   f62e0ace2eba5dce2a0f16879c9f36c9 missions/POE.MN3
      489       f9e2e24a8edc5e50f60a0e433e1f3b76 missions/POE.TXT
      2785303   f733e018175eb64e727ab0e15d4589e1 missions/Tri-Pod.mn3
      879       1898c24d55dff601067187866950d8b9 missions/Tri-Pod.txt
      1365321   cc2af5b0d7953690902ccbd03a93478f missions/bluedev.mn3
      633       b084b4cbf169d5e6d558b07bfb6eb8df missions/bluedev.txt
      16548904  0416c32f0df0c235c330ec0aa9f7a819 missions/bside.mn3
      15528976  78087034b83b380c896ca1b403015071 missions/bsidectf.mn3
      696113    5b6d01988928dc70200aa90c11cbab94 missions/kata12.mn3
      476       516f998969dbec3475b78f1eb0cdce1b missions/kata12.txt
      8569976   4dc5c4c46a022796c26692306421186a missions/merc.mn3
      614910    1fb59035424394b02fe303a170b82aa7 missions/stonecutter.mn3
      1280      11db259f419ef8c41aa0f81d5f6dfaa8 missions/stonecutter.txt
      4536784   055456e6eed151695e75bf0afbf4fb81 movies/ME.MVE
      31577424  4e6246804cd4207ef016637bf53ba4a4 movies/MI.MVE

  mercenary documentation:
    group_members: |
      2990      78a99b655a6f832c9900e19f0450712a README.mercenary

sha1sums: |
  00a9c77eb8de4548ea527e2fbe70c7b4342e525b lokishim.zip
  404c738c7ab456a266d8b67de502755e39a98bc3 descent3-1.4.0a-x86.run
  933eed84b8f7c21451f6293e62380c258b418056 descent3-1.4.0b-x86.run
  71d3643974ba2f126a33e01e39ed7db9948fb4e9 FAQ.txt
  556d7d4479d97543d2f2277e6fc6906568fc4727 README
  f43a26f1c95c22bf57a49cfd4cf462d9bfbf7f68 README.mercenary
  90e0f5edb53f720b3d789ca9eb775d5fb246b8cb bin/x86/glibc-2.1/descent3
  447eb63807f16077708c92eec03ea237958b54f1 bin/x86/glibc-2.1/nettest
  8c57db4fdf8bed7e0f96bdb9f21864ee5709e9e0 Dedicated.cfg
  9d96baaf11dea67015c626f817084699a0af5db6 d3.hog
  78fb84e890a3f7c0d5696ccca0bf7e4ef2a7a9f0 d3-linux.hog
  0488cdc83626ef4a85b17dd68435572f9c7d761c d3merc.pkg
  7ee4b0f38e9b5c94206e227f545ae1469ee72561 data.tar.gz
  efaee1a0b21b12bfda8c3b356c76dbf8d20ef89f demo/Secret2.dem
  66928e6cbb59c3a3bce606959ef4a865fe04e642 demo/empty
  4d88e2f78f40348b3c2885b7c4c41b92052783bf extra.hog
  4175bfe4a052b624cf28159d950a36581cf8b832 extra13.hog
  ed570a95c3d10425b26767face2820d2e79b6177 icon.bmp
  de1202ba9ec81be40133396be405cb9cb48dcdd2 icon.xpm
  4cb1065287c301f69bf6acc1511ee72b44319f5f imd.bmp
  39d56779ce4252464f999e83c261de4e963fb6dd merc.hog
  0a7d310672ef0222c60096d00909bd866d574785 missions/bedlam.mn3
  2b0729569118fa62bdc5962ec4afb987c2da9f43 missions/d3.mn3
  6554809cf0ea050e91a396de25ebf9aece167c8d missions/d3_2.mn3
  849863a3a0b4227cbfefa01c2b1459966c21eec1 missions/d3voice1.hog
  ff710e5424d9820d7bbd7be03de2d56a59727ab5 missions/d3voice2.hog
  ef235e78d7f0998a832410488d98870749ea3c16 missions/dementia.mn3
  933e02da6c21b4cc1181eb35c53533471057dff8 missions/frenzy.mn3
  f80a1b4b8700a244edfce648d89e874ce122a046 missions/fury.mn3
  9de2393ac9b1b306d63101608b95c13c5e253fd8 missions/training.mn3
  aeecf40238b433f6c884ff177539eef0e4d0cbd1 missions/CHAOS.MN3
  22033c3e975703221ed19317cd5440167715c0e7 missions/HAVOC.MN3
  97de1c0cbc34db7ae89f3b0d44a7fc0c7a15fc92 missions/MAYHEM.MN3
  c5d29632a69681d69e00ab8ae4943b7f26a603b7 missions/POE.MN3
  aa50d8b65783c20bb0acff325dc45ee41486d147 missions/POE.TXT
  f6bd5b6bc99b54c7801cfd38d4c9e12831326593 missions/Tri-Pod.mn3
  4a73849379ef64bd1c38f8042076f1bede5fec7a missions/Tri-Pod.txt
  0257fc3524966645ebd1d1e55fbaa54b6c5cf2c9 missions/bluedev.mn3
  d2609a74f5fd82debb94e2cc334fa4b466d37437 missions/bluedev.txt
  11c13d5583d18f09ca3066e3a21eb091ac802b98 missions/bside.mn3
  2357d7d75d19a0a7ae94297650046208c4131fff missions/bsidectf.mn3
  1b2e06278d88e2f40dfb804ffce500ae546a10e2 missions/kata12.mn3
  6fc35026268a126e7fb83e32697c2842dd772f11 missions/kata12.txt
  f11e8e22a3c912ec39645c83566d365b2af135c0 missions/merc.mn3
  864ac9833ad4de34db8c468ace7574e221bcf2e3 missions/stonecutter.mn3
  203d5e8cec61214c23b533a53c498a0595cb59ba missions/stonecutter.txt
  93643a2b4ae666262dc537c61eb99cf3e818bc52 movies/ME.MVE
  d0d734e631c2eeb91bf8c65ec803725e8ad4c86f movies/MI.MVE
  7b73b01eac48ce288dc69ba387c905f0f362c5b6 movies/end.mve
  5fb46d0cb2334d51f895fe6307d20e5451384c11 movies/intro.mve
  524fa5caddb14e955db004d4d70cea0bfd394ba9 movies/level1.mve
  0c5e62017e6663c80b708772274f3e1e37e0621c movies/level5.mve
  546e9b1e4e8527476a99e214ec04f1dc39624544 netgames/Anarchy.d3m
  a91eea86a3811bb393647fc1b60a81da48f28b11 netgames/co-op.d3m
  42ab191b91c2bf19a2a67f107fe651c52f8177a4 netgames/ctf.d3m
  7b70db594af22c3443782f2706c071c7755f0863 netgames/entropy.d3m
  7f386b237f1dc6aecc595eefcff805ac3b7616a1 netgames/hoard.d3m
  7213c775f0b7b564724432c39d2338f2c5db696a netgames/hyperanarchy.d3m
  8290251ee95307e87425180b5dd3cd23b41d24c3 netgames/monsterball.d3m
  ab7661ad9f3e8c5cd2a5af30558c06d60af28791 netgames/robo-anarchy.d3m
  b54992550fd4155e7d379010ce66670e9e19b3cf netgames/tanarchy.d3m
  56afa1114233f04e8f6c389e90dc06672541f340 online/Direct TCP~IP.d3c
  56afa1114233f04e8f6c389e90dc06672541f340 online/Parallax Online.d3c
  605c67c869fcf766e030b043b5c0eb03fb418bd8 ppics.hog
  e1127646ec76ebb89359d259a183eae5fdbd84ad shared.tar.gz
  ceaf166c44e5800400612c66c0861900766e4049 d3-linux.hog?140a
  a451d65321436a1db11e5721affe767a4c2099c2 d3-linux.hog.0?140a
  793f541023e8121a62d86dd74bbd07794532287c descent3?140a
  d5b0070b6e2577d65d94f3ab206d9c8a6c3e3533 descent3.0?140a
  3b1f370e3d18b60a5f0bd0e49052e26e3351c283 netgames/Anarchy.d3m?140a
  1d39ff7d1ce727a0691d9757ce8097759ba6de97 netgames/Anarchy.d3m.0?140a
  cd01c7376758ff7d31eebc338a62663b93790936 netgames/co-op.d3m?140a
  72ca0c2495e117ba3959ddadc448601336c4ad3f netgames/co-op.d3m.0?140a
  38edfb233b97c557105c2a4413306af0e92adacf netgames/ctf.d3m?140a
  0837ef1f9c211d1069eae0f6117197f1f415f074 netgames/ctf.d3m.0?140a
  52f220986010ef22b3884511514dca44281a66b0 netgames/entropy.d3m?140a
  781399f54d128e0c844a2ae96a9c749356f4bdd5 netgames/entropy.d3m.0?140a
  e0164eb15bff87fda92f61f055aade5ff8cf3d2b netgames/hoard.d3m?140a
  242ac454b2c1dbf2540265485a2f798c87875d0f netgames/hoard.d3m.0?140a
  d7754fc25b2480f198349222cc8333c65c486180 netgames/hyperanarchy.d3m?140a
  35e895c97da0842412536331023a893afb76f1fb netgames/hyperanarchy.d3m.0?140a
  f1e677aa41c5119ff1137e995204b7cd2eb6e607 netgames/monsterball.d3m?140a
  1007d86e970870acb7b8dd5dc20434bcb1dfd555 netgames/monsterball.d3m.0?140a
  2b1893dbacc80bd6ae969247099230be6dcd3a49 netgames/robo-anarchy.d3m?140a
  21c8000052ef54c1f151fae6cc932f9d23e291f3 netgames/robo-anarchy.d3m.0?140a
  cce6273f030f495d0e0a0d36b73d52021c1b781a netgames/tanarchy.d3m?140a
  e89d67435c7401230f15ba94f11c75f9bd35c7f6 netgames/tanarchy.d3m.0?140a
  b112db3f6aa66d581a018d28d750fdd3b7ab1bdf README.txt?140b
  0302cabf4db21f344b5889c8ef2047bfe682f6aa descent3.0?140b
  c449f924210baa49dfba80b0ac713419a4f21243 descent3.dynamic.0?140b
  e1be3c9cf24e452f4e5f0628bfbd7911f4d4b59a d3-linux.hog?140b
  2e15f83bf0ed74e89ac68cc8eeed0dc84ceab2fb d3-linux.hog.0?140b
  de194c1c36318449d2d6de8735ef777a769591af descent3?140b
  9c208b0d32019fe80d0b164010557025f12dd21d descent3.dynamic?140b
  8717816305fb8312628f346914762656fc0937bc netgames/Anarchy.d3m?140b
  1867f2899879260997300922bb0fd1ca2e17b668 netgames/Anarchy.d3m.0?140b
  1aac7652b3fcd28eecd29d9af785248e61d87e9b netgames/co-op.d3m?140b
  6ec1ce60427caf47362a10c574e348a8c4b47d53 netgames/co-op.d3m.0?140b
  99ab61a8d05b9a02b60d7be3562d1105ba9112ca netgames/ctf.d3m?140b
  a0c5f5b4817633d854994eb806a47f40253a852a netgames/ctf.d3m.0?140b
  89ca2afbf2d2a1c1af866985adf103eafb35d52a netgames/entropy.d3m?140b
  93bd5e027dca70cdd45d07a290529ac75c7f59eb netgames/entropy.d3m.0?140b
  d8f891bda8c94e2e83f97052d59b50b39b74824e netgames/hoard.d3m?140b
  2fff9aff3c29c49b74f8af69ded963b3680de7a7 netgames/hoard.d3m.0?140b
  9eb88309c23635c911672fcbf4342b193c6d1a89 netgames/hyperanarchy.d3m?140b
  8c6b2fba48b2079b2678ea39bbe3962ae5802965 netgames/hyperanarchy.d3m.0?140b
  c74ef90729394d2691f47ddf0b6e0e5fde8e6446 netgames/monsterball.d3m?140b
  1d5742fe3c54cdc918834f453e5e41895c8cd343 netgames/monsterball.d3m.0?140b
  ccd531835565250481a8fbcb205863f302031c25 netgames/robo-anarchy.d3m?140b
  f5568d42d2b9b4c6252e0c79c6156041d07896aa netgames/robo-anarchy.d3m.0?140b
  ba551afcbc46bf0de7811dd82cc91d8f6d83c983 netgames/tanarchy.d3m?140b
  7133134874ca2d16a7e838097255ad2284a03afa netgames/tanarchy.d3m.0?140b

sha256sums: |
  0146067f941cf153954537cee887ff583cbe564d5b78c7fea5db9917b5bab846 lokishim.zip
  fe894b1cbcb16ec813c368f16549c38a3cd65514494f0be0b3095e936c9f67fe descent3-1.4.0a-x86.run
  4c56ab91c332b0ccdc052fa3dd1c3a72cda204724bc98429cda22379269720e1 descent3-1.4.0b-x86.run
  16ed34370ee88f96ecd1b817b4f98d0db2d315e841dd1da9cc7729eff66b98fb FAQ.txt
  5aec12f6cb139f345bd45ea155200afea5971fb9e426af4f1692432743a1097c README
  6299bdaa58ae369fae73b5cf34d429a2e963ba3b59537e0297f82969650ae889 README.mercenary
  63ee69c3b62fcb669b042f8d2de0702aa8523d85f376ab7d97d3dd759da9d93a bin/x86/glibc-2.1/descent3
  d85722af3ea1d1a8e2611b6dfbab878cbd2cf23a48c4b9e65c8121852b9443e4 bin/x86/glibc-2.1/nettest
  a1b541a492dfccb6f260e6a07faf5ba70bfe86e019784eabc856431d6bf073e3 Dedicated.cfg
  a0f1cb2c1a73da828a5fd4e80d6544b63da04e177dc2b894d9e6418296bc24c6 d3.hog
  1da986ac20e0ef99107bfaa7efe2df4ba5efb7c71f572a88def60a3c012218f3 d3-linux.hog
  6fded1c7ed4137611a1b79c42dbc5bfad53ce50b82a50310c8032bab35e2d456 d3merc.pkg
  b8f757b8aa518e1dad02432429a5919b5ff33189560a108b3db36962d5090e4e data.tar.gz
  fb319919b1e67d150f9549f6ebdc9e060bd91246320c4a64b2535fd4d410f3d7 demo/Secret2.dem
  dba5166ad9db9ba648c1032ebbd34dcd0d085b50023b839ef5c68ca1db93a563 demo/empty
  005f035b83d7523e883b4d1b4a103367ac3b7ef45977f18c1ffc89e2f8964df9 extra.hog
  bd257272cbb436b78cacdc8e8ae8f8260922cd023bae88ff848ee89b90807187 extra13.hog
  53fd9340ab22de5b2529fb0da96e5615714e21015dd6739b179aab383903af7e icon.bmp
  38253b0e5ab914267217b53eca46e0408cfaf9e088f0673612c5ea173cdc6f42 icon.xpm
  46f855656fd489aaf966b5673649bb07abeb5c6feda4f3dfaba8580895ed2bee imd.bmp
  29c9188697aafc8b2c33ebe8c9a56f2bc0e8742860def769a747a39e29b6350c merc.hog
  9286b2c1c2551b762bc4efda8ac0d58077a139bfd60c884ea1bb495b88c11bb4 missions/bedlam.mn3
  ee341d6f89b8aa464d963eada52babbd9b71843444ffa49b24f47fd31f6fdd1a missions/d3.mn3
  c718fa60615dcaba1d8f42251e430e02c1f0a635a3449a2046e647150f50f8fb missions/d3_2.mn3
  40edf008c428fab18f942f654fd370f0af17422086ad7b9fb5e43473c7bd8b8b missions/d3voice1.hog
  65bed20baa47bc09fa8c808e49622cfeeaac7bf655d9459ba6964ba0c1ce0668 missions/d3voice2.hog
  93bbeb703f9b002966b24ed80891166368767bedad567fb9b695e2aba1638ebe missions/dementia.mn3
  cab085c50126bd0dc97a3177808c94df836ad302224e08aa51169625d73badd1 missions/frenzy.mn3
  a7c3084f66890e54e730202f1b75e2e8a04291b4edc35257b4a2639f84bdb1f0 missions/fury.mn3
  fc1d81921cc4b2618e441b7b9d08c4bcb5cff90731be1bfa6f3a7b054fc0cb54 missions/training.mn3
  41094116ff90133141cb0c812fe66cb7988786892971552fde4aaa8ff12b28b4 missions/CHAOS.MN3
  66a9a55e5c4348f7f84945b8a373e84928e8c71245b40ad34db99d8a0f2e9418 missions/HAVOC.MN3
  1d131bdd2b5c988b1d48a447e9f8b0d3d2e4ede7f9751135db0800faac434a7a missions/MAYHEM.MN3
  4b90af64dc23f23d84e6e6088397698db5b496b0a16d6e762db8cf22862f34a6 missions/POE.MN3
  eb19155a8cb663fa1ae23ea2a2cb75d5283fb5e55df026cc85975ccb5432cfde missions/POE.TXT
  437b70c100aef5f643715b922abea23e1bc12e1d9866ee924bf046c0205d3e4a missions/Tri-Pod.mn3
  19f265520f32a967fc6258580bcc14d9b5856ed01e0aa3a47eb71b1469c4c430 missions/Tri-Pod.txt
  2a05a33bfd25bab50fa80e9af1650d8dfe8f77dca5324c678f2637d20e3965d2 missions/bluedev.mn3
  c05c09ddb8d2cc50191d70706055b8ce3499ed6ffe8d552a4779d24f727e8d79 missions/bluedev.txt
  c2b319b1875e0a4ac6b6e3fdf27203cbefb60997ae33ee90e5a88f7a25af1a79 missions/bside.mn3
  dd7529f96e7a9f694a2b6b76b3fe30458acbf2fe53b97abefd3507e1520f1fcc missions/bsidectf.mn3
  ce0b17c76c7f8d82cad962494e735ab7f71d79c65b6d104926e061180f901359 missions/kata12.mn3
  5e24feb9996a1e345fb224b613b51cb657a61be42888e677ce71f81a13370959 missions/kata12.txt
  4c375abf2692706e62ee58aeac5cda6105ab51c868ec93d4a823d257e0811979 missions/merc.mn3
  9bc3e97bbcf5c86e25d6d9162d3d3c7697ebcedd024d71a9c97ab91f236fbdc7 missions/stonecutter.mn3
  7d274b2a31b5acc8367713ce537a45317633606b7b74be2b0e3bcfd2c1f6d7f2 missions/stonecutter.txt
  4c03d7eb130f138a2f1fe8ad8e039a836ebf16242d06431eb45d12e7770bcfbe movies/ME.MVE
  e85d540846a1fe8a7bf62aeb5a0fdf56c19e0e1fb69e7444c333f4acceb56b9c movies/MI.MVE
  e53ce2c2da2bd44a5326a8ce9a036c222a7a7ee429a54e232cf15a7ed424a556 movies/end.mve
  98516324b7bbbd8d895c23f1bc98644bf35c0c910f73c2e04da906e5c0f7ec5d movies/intro.mve
  0aec456695a3992886546662b70d13169ca5a88bab5f3edb751c9e62e80de9ff movies/level1.mve
  ea8166585d33cad2075ef10117996f6113d2f4fb558787967205a7bbb08178b9 movies/level5.mve
  c3385468cf62ae721487f3f6b90459760202c4eef947fa5a387082cd1d761e2c netgames/Anarchy.d3m
  352ba7291e8d2b02f3a1202239fd8cc343eaf6f5edfe2e31b8f705d93530a057 netgames/co-op.d3m
  63f3fdbd672498157bbad7f71074b514bb55f944095dcd5fce31787e1b1c27df netgames/ctf.d3m
  3ba26c8ff8dc23479a53192df8a04e9266bb9dbd53f20c58e193ecc6a4fa1ae2 netgames/entropy.d3m
  a0c8421d4898742c7e46cb0ad2a0e1bb96440a601169c1941c495f6d2785e844 netgames/hoard.d3m
  f6983f591aa4d7c2253c03076ba5a458652e62a3cede495980ef0035ae329d44 netgames/hyperanarchy.d3m
  46146aef93bf31ccb5c78be7a6b420ed62dad6797603d0c311b5a148a79c44a9 netgames/monsterball.d3m
  97acc644f29e7419cbc65311773ba1dbd69c72e66055078c82cd939618614355 netgames/robo-anarchy.d3m
  1c831f2853b3d628570f5ce61c5d82c201a3d24870c149b8d33162fee2b74742 netgames/tanarchy.d3m
  372a6ec359387800c8ce36bf883ac14de36255ec88c258e7a57b34428754bdc3 online/Direct TCP~IP.d3c
  372a6ec359387800c8ce36bf883ac14de36255ec88c258e7a57b34428754bdc3 online/Parallax Online.d3c
  6e911c554b42a04a4ddb2ad24676a62ce7026a07ed74e54043d5bd459dea2546 ppics.hog
  cf7d4f5f1c961860d53efccd8f95ebb69ce255e2b99152dc5a9b9456dc7c0c08 shared.tar.gz
  b6eee4a787fa356b0588fcbe98bc5996df77dfe5eb50dc9eba81518b38cbec16 d3-linux.hog?140a
  f3d9d07cf51afe23ee4b9f0b54ccc115bd14630ed05213b9e2da319bd7127870 d3-linux.hog.0?140a
  7b048250d84c8872469f425a0a41a971ed130ed5bd37f4c6802e221aec8bd0d4 descent3?140a
  46171ae39636ba5c23b58bd5033d1293197d7adeaa5483df2cf9b7382fb8b3d6 descent3.0?140a
  aa6986e66f1cf0ccbe5e8ac372517026969406c694b1ba3c7439d4b55dc9d761 netgames/Anarchy.d3m?140a
  fb4e510a88a32ae95cf556ca063303adc18ed3d49dbf796cd3c5e70909bc8482 netgames/Anarchy.d3m.0?140a
  392ba3bf2040a3dc8b5fcb3b91bafe44d08ae45e657935cc28f86bb8849120df netgames/co-op.d3m?140a
  caebc4ed959208064e8804ce9f68c0ef67aa783cfa769951cb852826bb429c23 netgames/co-op.d3m.0?140a
  109f982bd7f8adf46b2a966d2f7bc025889e1d47dc6f4a279d8a5fb3ccf22710 netgames/ctf.d3m?140a
  19dfd7fc39b72e07e1174ae8366addef3f2ace68294509c98be92ddad44bc4fa netgames/ctf.d3m.0?140a
  26cd15b9c3e99446156f046706fe149e42ab8879316561f33442c1f81fdfec4e netgames/entropy.d3m?140a
  8926f63d58d1e0e614846c43a5b66cffb43be688b1614df1eccc13aa0286534d netgames/entropy.d3m.0?140a
  bbfc78597c74b66cacb9b93f2645b4d1e3ad56878192394a7e5fbefa7942db62 netgames/hoard.d3m?140a
  d072fe55fde1c6a8584444fe8dc55d912aeb69ce4f3e470ff39631e2b9367e56 netgames/hoard.d3m.0?140a
  6b4c654864d8b2f0effa289920ed7490e4649e889e4ec426ba68c6c5bc15ea69 netgames/hyperanarchy.d3m?140a
  1c3deecd142f10fc2708b4cdda646fc49d97d54209ae5521af74c6dd919bf716 netgames/hyperanarchy.d3m.0?140a
  653bbb31433055ed7de1ce18c7a16029d4b844782a6db952882c15512d895a64 netgames/monsterball.d3m?140a
  6d54f4016892860ac28c1aa4bdf3ad053237521b63f8ab4e51dc661d3e287c7a netgames/monsterball.d3m.0?140a
  65056855e768421ff20221e3dad4963109ed03a12f52847e2271d4cee9730031 netgames/robo-anarchy.d3m?140a
  5b043cdf6857ab3f9cb7eb0182fb96948ab3b133891ddbc3e639aba41a57b92e netgames/robo-anarchy.d3m.0?140a
  ccb28ef5ed338177761881321b66b3a922556e8ebeab775c4f9260818520cca7 netgames/tanarchy.d3m?140a
  ef7e24499768216c7df776cd05fd3d542c66d1ce133d534fc27bb585f50ca056 netgames/tanarchy.d3m.0?140a
  d49bf0039573ff5f6330331a8221f5c287d4f585a04f197629d2a37dc97ee0dc README.txt?140b
  402af40b8d8b3a41286d3f41f5c03f0d7efa00ddf1d0c456332cb94f444c4fbc d3-linux.hog?140b
  e31ab12e28051e6fd9a5c035b433b1c608da7a3bdcdb8d6adf3f6c2818178d92 d3-linux.hog.0?140b
  dfe1be5d5627e45120cd693adf996de79bbf82809b16bafbf27074f6e99da05b descent3?140b
  2927baa003924912289132fdad214ab353b7c72790649f2200c726f907b9be2d descent3.0?140b
  c78e946d13b0f45f5b39ed646f1e2cb88974d3ba272127a629dd258868703e0b descent3.dynamic?140b
  f550349ba9c5adfad59cb8318fb3ad9a591c5582d083174452bfe231b36e3248 descent3.dynamic.0?140b
  7f4b5ade2c944fdae7e21a7fbe074538ec4c3e90af0f0f84c1144e15e381f6cc netgames/Anarchy.d3m?140b
  164755d0898501de070b7e73147fe9ebe26e36fb2b1cd027a923b93bef5ec7ce netgames/Anarchy.d3m.0?140b
  3fd260b7581d07025651856035b142396e25e0eaeedaf57fa1d8b1d7f99a8d6d netgames/co-op.d3m?140b
  57544205dde934da3e7989750a2e1332604b975d74bbfe3132d048dddbbd5106 netgames/co-op.d3m.0?140b
  3a8e1827bdd970cb236b8f745bff27e6a6ee1313f4ef9ef6c7356365f27c383e netgames/ctf.d3m?140b
  725fcba384932049abaa215bf522d7c27f9a8375b25501f41bba1b44d1cf347d netgames/ctf.d3m.0?140b
  207e6b6181176cc00798b42269fddde2f5732b54c0ba5a5a43fa1b1ddca0314b netgames/entropy.d3m?140b
  04666a5863edeeeb64bcd13e2b598dd06b217f18b30abf28d71749b285815fbd netgames/entropy.d3m.0?140b
  3d92d83f86e82c5992093a31fe4f7c1095ff9b50ab9f069f115ebe74b5249ded netgames/hoard.d3m?140b
  aff602648b1117d50d9671466230a4d4175f301226592f58dd0c51b752f181f6 netgames/hoard.d3m.0?140b
  ff03da484965491f4a82c91e6392bcc48b07c05d3903dc00760a710103d9bcd5 netgames/hyperanarchy.d3m?140b
  85e5245639e35553f8af36cfc654d3c338af8ae631e543eae5b83d10353e758b netgames/hyperanarchy.d3m.0?140b
  bfd457a4197c9ed845794deb1dceabfb1c6a24f2ccf73950d43627e5d3bc7458 netgames/monsterball.d3m?140b
  fff41c2cd51aaea92314a06c5aa9ac7bde9ad3155177e6f1da2fe3db95ebc536 netgames/monsterball.d3m.0?140b
  1485b01f84d7d51dd90d57f607403d529e7a777fec8570c4aea6907dde2ffea4 netgames/robo-anarchy.d3m?140b
  f6121f4b185b518dc002b58e558306fa3658598fc21c91fa628cc32a0d775ca1 netgames/robo-anarchy.d3m.0?140b
  387e6d3c9bc816fd9dd86417ef1a52150b617ff64090af0f66c1125706ab8fbd netgames/tanarchy.d3m?140b
  df219edef3850b06358e749518a116d5f30ae2eb628c7bfce59cd617240e7f00 netgames/tanarchy.d3m.0?140b

...
