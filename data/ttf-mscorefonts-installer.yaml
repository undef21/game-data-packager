wikipedia: https://en.wikipedia.org/wiki/Core_fonts_for_the_Web
copyright: "\xA9 1996 Microsoft"
longname: "Microsoft Core Fonts for the Web"
help_text: |
  This package allows for easy installation of the Microsoft True Type
  Core Fonts for the Web on devices without an Internet connection.
  .
  It will reuse & shadow the files previously downloaded by the
  "ttf-mscorefonts-installer" package in "contrib" section,
  if any.
  .
  NOTE: the package fonts-liberation contains free variants of the Times,
  Arial and Courier fonts. It's better to use those instead unless you
  specifically need one of the other fonts from this package.

packages:
  ttf-mscorefonts-installer:
    section: fonts
    recommends:
      deb: "fonts-liberation"
    provides:
      deb: "msttcorefonts"
    install:
    - assets
    license:
    - licen.txt
    install_to: /usr/share/fonts/truetype/msttcorefonts
    symlinks:
      $install_to/andalemo.ttf: $install_to/Andale_Mono.ttf
      $install_to/arial.ttf: $install_to/Arial.ttf
      $install_to/arialbd.ttf: $install_to/Arial_Bold.ttf
      $install_to/arialbi.ttf: $install_to/Arial_Bold_Italic.ttf
      $install_to/ariali.ttf: $install_to/Arial_Italic.ttf
      $install_to/ariblk.ttf: $install_to/Arial_Black.ttf
      $install_to/comic.ttf: $install_to/Comic_Sans_MS.ttf
      $install_to/comicbd.ttf: $install_to/Comic_Sans_MS_Bold.ttf
      $install_to/cour.ttf: $install_to/Courier_New.ttf
      $install_to/courbd.ttf: $install_to/Courier_New_Bold.ttf
      $install_to/courbi.ttf: $install_to/Courier_New_Bold_Italic.ttf
      $install_to/couri.ttf: $install_to/Courier_New_Italic.ttf
      $install_to/georgia.ttf: $install_to/Georgia.ttf
      $install_to/georgiab.ttf: $install_to/Georgia_Bold.ttf
      $install_to/georgiai.ttf: $install_to/Georgia_Italic.ttf
      $install_to/georgiaz.ttf: $install_to/Georgia_Bold_Italic.ttf
      $install_to/impact.ttf: $install_to/Impact.ttf
      $install_to/times.ttf: $install_to/Times_New_Roman.ttf
      $install_to/timesbd.ttf: $install_to/Times_New_Roman_Bold.ttf
      $install_to/timesbi.ttf: $install_to/Times_New_Roman_Bold_Italic.ttf
      $install_to/timesi.ttf: $install_to/Times_New_Roman_Italic.ttf
      $install_to/trebuc.ttf: $install_to/Trebuchet_MS.ttf
      $install_to/trebucbd.ttf: $install_to/Trebuchet_MS_Bold.ttf
      $install_to/trebucbi.ttf: $install_to/Trebuchet_MS_Bold_Italic.ttf
      $install_to/trebucit.ttf: $install_to/Trebuchet_MS_Italic.ttf
      $install_to/verdana.ttf: $install_to/Verdana.ttf
      $install_to/verdanab.ttf: $install_to/Verdana_Bold.ttf
      $install_to/verdanai.ttf: $install_to/Verdana_Italic.ttf
      $install_to/verdanaz.ttf: $install_to/Verdana_Bold_Italic.ttf
      $install_to/webdings.ttf: $install_to/Webdings.ttf

files:
    Andale_Mono.ttf:
      look_for: [andalemo.ttf]

    andale32.exe:
      download: https://downloads.sourceforge.net/corefonts/andale32.exe
      unpack:
        format: cabextract
      provides:
      - Andale_Mono.ttf

    Arial_Black.ttf:
      look_for: [ariblk.ttf]

    arialb32.exe:
      download: https://downloads.sourceforge.net/corefonts/arialb32.exe
      unpack:
        format: cabextract
      provides:
      - Arial_Black.ttf

    Arial_Bold.ttf:
      look_for: [arialbd.ttf]
    Arial_Bold_Italic.ttf:
      look_for: [arialbi.ttf]
    Arial_Italic.ttf:
      look_for: [ariali.ttf]

    arial32.exe:
      download: https://downloads.sourceforge.net/corefonts/arial32.exe
      unpack:
        format: cabextract
      provides:
      - Arial.ttf
      - Arial_Bold.ttf
      - Arial_Bold_Italic.ttf
      - Arial_Italic.ttf


    Comic_Sans_MS.ttf:
      look_for: [comic.ttf]

    Comic_Sans_MS_Bold.ttf:
      look_for: [comicbd.ttf]

    comic32.exe:
      download: https://downloads.sourceforge.net/corefonts/comic32.exe
      unpack:
        format: cabextract
      provides:
      - Comic_Sans_MS.ttf
      - Comic_Sans_MS_Bold.ttf

    Courier_New.ttf:
      look_for: [cour.ttf]
    Courier_New_Bold.ttf:
      look_for: [courbd.ttf]
    Courier_New_Italic.ttf:
      look_for: [couri.ttf]
    Courier_New_Bold_Italic.ttf:
      look_for: [courbi.ttf]

    courie32.exe:
      download: https://downloads.sourceforge.net/corefonts/courie32.exe
      unpack:
        format: cabextract
      provides:
      - Courier_New.ttf
      - Courier_New_Bold.ttf
      - Courier_New_Italic.ttf
      - Courier_New_Bold_Italic.ttf

    Georgia_Bold.ttf:
      look_for: [georgiab.ttf]
    Georgia_Italic.ttf:
      look_for: [georgiai.ttf]
    Georgia_Bold_Italic.ttf:
      look_for: [georgiaz.ttf]

    georgi32.exe:
      download: https://downloads.sourceforge.net/corefonts/georgi32.exe
      unpack:
        format: cabextract
      provides:
      - Georgia.ttf
      - Georgia_Bold.ttf
      - Georgia_Italic.ttf
      - Georgia_Bold_Italic.ttf

    impact32.exe:
      download: https://downloads.sourceforge.net/corefonts/impact32.exe
      unpack:
        format: cabextract
      provides:
      - Impact.ttf

    Times_New_Roman.ttf:
      look_for: [times.ttf]
    Times_New_Roman_Bold.ttf:
      look_for: [timesbd.ttf]
    Times_New_Roman_Bold_Italic.ttf:
      look_for: [timesbi.ttf]
    Times_New_Roman_Italic.ttf:
      look_for: [timesi.ttf]

    times32.exe:
      download: https://downloads.sourceforge.net/corefonts/times32.exe
      unpack:
        format: cabextract
      provides:
      - Times_New_Roman.ttf
      - Times_New_Roman_Bold.ttf
      - Times_New_Roman_Bold_Italic.ttf
      - Times_New_Roman_Italic.ttf

    Trebuchet_MS.ttf:
      look_for: [trebuc.ttf]
    Trebuchet_MS_Bold.ttf:
      look_for: [trebucbd.ttf]
    Trebuchet_MS_Italic.ttf:
      look_for: [trebucit.ttf]
    Trebuchet_MS_Bold_Italic.ttf:
      look_for: [trebucbi.ttf]

    trebuc32.exe:
      download: https://downloads.sourceforge.net/corefonts/trebuc32.exe
      unpack:
        format: cabextract
      provides:
      - Trebuchet_MS.ttf
      - Trebuchet_MS_Bold.ttf
      - Trebuchet_MS_Italic.ttf
      - Trebuchet_MS_Bold_Italic.ttf

    Verdana_Bold.ttf:
      look_for: [verdanab.ttf]
    Verdana_Italic.ttf:
      look_for: [verdanai.ttf]
    Verdana_Bold_Italic.ttf:
      look_for: [verdanaz.ttf]

    verdan32.exe:
      download: https://downloads.sourceforge.net/corefonts/verdan32.exe
      unpack:
        format: cabextract
      provides:
      - Verdana.ttf
      - Verdana_Bold.ttf
      - Verdana_Italic.ttf
      - Verdana_Bold_Italic.ttf

    webdin32.exe:
      download: https://downloads.sourceforge.net/corefonts/webdin32.exe
      unpack:
        format: cabextract
      provides:
      - Webdings.ttf
      - licen.txt

    licen.txt:
      size: 4919
      md5: ebe2e15f556ab890d315e32166efe2ae

groups:
  assets:
    group_members: |
      105468    663974c9fe3ba55b228724fd4d4e445f Andale_Mono.ttf
      275572    f11c0317db527bdd80fa0afa04703441 Arial.ttf
      117028    3e7043e8125f1c8998347310f2c315bc Arial_Black.ttf
      286620    34cd8fd9e4fae9f075d4c9a2c971d065 Arial_Bold.ttf
      224692    a2b3bcdb39097b6aed17a766652b92b2 Arial_Bold_Italic.ttf
      206132    25633f73d92a0646e733e50cf2cc3b07 Arial_Italic.ttf
      126364    a50f9c96a76356e3d01013e0b042989f Comic_Sans_MS.ttf
      111476    81d64ec3675c4adc14e9ad2c5c8103a7 Comic_Sans_MS_Bold.ttf
      302688    20f23317e90516cbb7d38bd53b3d1c5b Courier_New.ttf
      311508    7d94f95bf383769b51379d095139f2d7 Courier_New_Bold.ttf
      234788    da414c01f951b020bb09a4165d3fb5fa Courier_New_Bold_Italic.ttf
      244156    167e27add66e9e8eb0d28a1235dd3bda Courier_New_Italic.ttf
      142964    f4b306eed95aa7d274840533be635532 Georgia.ttf
      139584    c61b355a5811e56ed3d7cea5d67c900e Georgia_Bold.ttf
      158796    e5d52bbfff45e1044381bacb7fc8e300 Georgia_Bold_Italic.ttf
      156668    1e4e5d1975bdf4a5c648afbf8872fa13 Georgia_Italic.ttf
      136076    8fc622c3a2e2d992ec059cca61e3dfc0 Impact.ttf
      330412    4f97f4d6ba74767259ccfb242ce0e3f7 Times_New_Roman.ttf
      333900    ed6e29caf3843142d739232aa8642158 Times_New_Roman_Bold.ttf
      238612    6d2bd425ff00a79dd02e4c95f689861b Times_New_Roman_Bold_Italic.ttf
      247092    957dd4f17296522dead302ab4fcdfa8d Times_New_Roman_Italic.ttf
      126796    70e7be8567bc05f771b59abd9d696407 Trebuchet_MS.ttf
      123828    055460df9ab3c8aadd3330bd30805f11 Trebuchet_MS_Bold.ttf
      131188    fb5d68cb58c6ad7e88249d65f6900740 Trebuchet_MS_Bold_Italic.ttf
      139288    8f308fe77b584e20b246aa1f8403d2e9 Trebuchet_MS_Italic.ttf
      139640    3ba52ab1fa0cd726e7868e9c6673902c Verdana.ttf
      136032    a2b4dc9afc18e76cfcaa0071fa7cd0da Verdana_Bold.ttf
      153324    f7310c29df0070530c48a47f2dca9014 Verdana_Bold_Italic.ttf
      154264    24b3a293c865a2c265280f017fb24ba5 Verdana_Italic.ttf
      118752    1a56b45a66b07b4c576d5ead048ed992 Webdings.ttf

  archives:
    group_members: |
      198384    cbdc2fdd7d2ed0832795e86a8b9ee19a andale32.exe
      554208    9637df0e91703179f0723ec095a36cb5 arial32.exe
      168176    c9089ae0c3b3d0d8c4b0a95979bb9ff0 arialb32.exe
      246008    2b30de40bb5e803a0452c7715fc835d1 comic32.exe
      646368    4e412c772294403ab62fb2d247d85c60 courie32.exe
      392440    4d90016026e2da447593b41a8d8fa8bd georgi32.exe
      173288    7907c7dd6684e9bade91cff82683d9d7 impact32.exe
      661728    ed39c8ef91b9fb80f76f702568291bd5 times32.exe
      357200    0d7ea16cac6261f8513a061fbfcdb2b5 trebuc32.exe
      351992    12d2a75f8156e10607be1eaa8e8ef120 verdan32.exe
      185072    230a1d13a365b22815f502eb24d9149b webdin32.exe

sha1sums: |
  953839b69c5bc047cb53efefc755dd21175a3a1d Andale_Mono.ttf
  2c5cb7cfa19eea5d90c375dc0f9f8e502ea97f0c Arial.ttf
  49560b47dac944923a6c918c75f27fbe8a3054c5 Arial_Black.ttf
  0d99c9d151db525c20b8209b9f0ee1ce812a961c Arial_Bold.ttf
  0aba42fc6e5b1e78992414f5c4df31376f90f0e2 Arial_Bold_Italic.ttf
  d205d443f4431600378adaa92a29cc0396508919 Arial_Italic.ttf
  d17fae7a6628e2bc4c31a2074b666a775eed9055 Comic_Sans_MS.ttf
  5518f0bdebe7212d10492ab6f4fff9b0230c3cbe Comic_Sans_MS_Bold.ttf
  9c5be4c1f151257798602aa74a7937dcead5db1f Courier_New.ttf
  39b43bf424ac193259b3787764c1cdd7a7e2242c Courier_New_Bold.ttf
  c5f4818fa6876e93f043a209597bcb39c57e43ca Courier_New_Bold_Italic.ttf
  74941cc95734772f8b17aeec33e9a116f94a26ae Courier_New_Italic.ttf
  5d69d55862471d18f1c22132a44f05291134cbf4 Georgia.ttf
  3ccf584caad7dfaf07a2b492e6e27dfe642c6ba0 Georgia_Bold.ttf
  328b246b57108d5f175eb9a4df8f613b7207d0bf Georgia_Bold_Italic.ttf
  f6bafcca21153f02b4449fc8949cdd5786bb2992 Georgia_Italic.ttf
  1cc231f6ba7e2c141e28db4eac92b211d4033df6 Impact.ttf
  d9d9ad4ba85fcd9dbe69e7866613756f1dbb4e97 Times_New_Roman.ttf
  f67a30f4db2ff469ed5b2c9830d031cb4b3174b4 Times_New_Roman_Bold.ttf
  e997a0bf7a322c7ba5d4bde9251129dee3f66119 Times_New_Roman_Bold_Italic.ttf
  5f896ef096ad01f495cefc126e963b2cd6638fab Times_New_Roman_Italic.ttf
  6480f383a9cd92c8d75ac11ef206c53e3233abb2 Trebuchet_MS.ttf
  b54b5fa32a884b4297b2343efdc745d0755cc7d1 Trebuchet_MS_Bold.ttf
  bc377a42afee7f73f0b80e2ed6e0d18edbd4f8fd Trebuchet_MS_Bold_Italic.ttf
  2614ce1c336f8568b9bf0c14752edfe1819a072f Trebuchet_MS_Italic.ttf
  ba19d57e11bd674c1d8065e1736454dc0a051751 Verdana.ttf
  fe5e9cfe72f1cbf07b4190f7fc4702cd15f452d1 Verdana_Bold.ttf
  09aff891c626fe7d3b878f40a6376073b90d4fde Verdana_Bold_Italic.ttf
  3ac316b55334e70a6993ded91328682733c4d133 Verdana_Italic.ttf
  bc1382a14358f747cbd3ff54676950231f67c95a Webdings.ttf

  c4db8cbe42c566d12468f5fdad38c43721844c69 andale32.exe
  6d75f8436f39ab2da5c31ce651b7443b4ad2916e arial32.exe
  d45cdab84b7f4c1efd6d1b369f50ed0390e3d344 arialb32.exe
  2371d0327683dcc5ec1684fe7c275a8de1ef9a51 comic32.exe
  06a745023c034f88b4135f5e294fece1a3c1b057 courie32.exe
  90e4070cb356f1d811acb943080bf97e419a8f1e georgi32.exe
  86b34d650cfbbe5d3512d49d2545f7509a55aad2 impact32.exe
  20b79e65cdef4e2d7195f84da202499e3aa83060 times32.exe
  50aab0988423efcc9cf21fac7d64d534d6d0a34a trebuc32.exe
  f5b93cedf500edc67502f116578123618c64a42a verdan32.exe
  2fb4a42c53e50bc70707a7b3c57baf62ba58398f webdin32.exe

sha256sums: |
  48d9bc613917709d3b0e0f4a6d4fe33a5c544c5035dffe9e90bc11e50e822071 Andale_Mono.ttf
  35c0f3559d8db569e36c31095b8a60d441643d95f59139de40e23fada819b833 Arial.ttf
  dad7c04acb26e23dfe4780e79375ca193ddaf68409317e81577a30674668830e Arial_Black.ttf
  4044aa6b5bebbc36980206b45b0aaaaa5681552a48bcadb41746d5d1d71fd7b4 Arial_Bold.ttf
  2f371cd9d96b3ac544519d85c16dc43ceacdfcea35090ee8ddf3ec5857c50328 Arial_Bold_Italic.ttf
  70ade233175a6a6675e4501461af9326e6f78b1ffdf787ca0da5ab0fc8c9cfd6 Arial_Italic.ttf
  b82c53776058f291382ff7e008d4675839d2dc21eb295c66391f6fb0655d8fc0 Comic_Sans_MS.ttf
  873361465d994994762d0b9845c99fc7baa2a600442ea8db713a7dd19f8b0172 Comic_Sans_MS_Bold.ttf
  6715838c52f813f3821549d3f645db9a768bd6f3a43d8f85a89cb6875a546c61 Courier_New.ttf
  edf8a7c5bfcac2e1fe507faab417137cbddc9071637ef4648238d0768c921e02 Courier_New_Bold.ttf
  66dbfa20b534fba0e203da140fec7276a45a1069e424b1b9c35547538128bbe8 Courier_New_Bold_Italic.ttf
  f3f6b09855b6700977e214aab5eb9e5be6813976a24f894bd7766e92c732fbe1 Courier_New_Italic.ttf
  7d0bb20c632bb59e81a0885f573bd2173f71f73204de9058feb68ce032227072 Georgia.ttf
  82d2fbadb88a8632d7f2e8ad50420c9fd2e7d3cbc0e90b04890213a711b34b93 Georgia_Bold.ttf
  c983e037d8e4e694dd0fb0ba2e625bca317d67a41da2dc81e46a374e53d0ec8a Georgia_Bold_Italic.ttf
  1523f19bda6acca42c47c50da719a12dd34f85cc2606e6a5af15a7728b377b60 Georgia_Italic.ttf
  00f1fc230ac99f9b97ba1a7c214eb5b909a78660cb3826fca7d64c3af5a14848 Impact.ttf
  4e98adeff8ccc8ef4e3ece8d4547e288ff85fdc9c7ca711a4599c234874bbe86 Times_New_Roman.ttf
  4357b63cef20c01661a53c5dae70ffd20cb4765503aaed6d38b17a57c5a90bff Times_New_Roman_Bold.ttf
  192e1b0d18e90334e999a99f8c32808d6a2e74b3698b8cd90c943c2249a46549 Times_New_Roman_Bold_Italic.ttf
  c25ae529b4cecdbca148b6ccb862ee0abad770af8b1fd29c8dba619d1b8da78a Times_New_Roman_Italic.ttf
  ec3ffb302488251e1b67fb09dd578b364c5339e27c1cfb26eb627666236453d0 Trebuchet_MS.ttf
  f65941f9487c0a0a3b7445996ecbbd24466d7ae76ea2a597ced55f438fa63838 Trebuchet_MS_Bold.ttf
  c0a6bdf31f9f2953b2f08a0c1734c892bc825f0fb17c604d420f7acf203a213b Trebuchet_MS_Bold_Italic.ttf
  db56fdac7d3ba20b7aededcb6ee86c46687489d17b759e1708ea4e2d21e38410 Trebuchet_MS_Italic.ttf
  96ed14949ca4b7392cff235b9c41d55c125382abbe0c0d3c2b9dd66897cae0cb Verdana.ttf
  c8f5065ba91680f596af3b0378e2c3e713b95a523be3d56ae185ca2b8f5f0b23 Verdana_Bold.ttf
  698e220f48f4a40e77af7eb34958c8fd02f1e18c3ba3f365d93bfa2ed4474c80 Verdana_Bold_Italic.ttf
  91b59186656f52972531a11433c866fd56e62ec4e61e2621a2dba70c8f19a828 Verdana_Italic.ttf
  10d099c88521b1b9e380b7690cbe47b54bb19396ca515358cfdc15ac249e2f5d Webdings.ttf

  0524fe42951adc3a7eb870e32f0920313c71f170c859b5f770d82b4ee111e970 andale32.exe
  85297a4d146e9c87ac6f74822734bdee5f4b2a722d7eaa584b7f2cbf76f478f6 arial32.exe
  a425f0ffb6a1a5ede5b979ed6177f4f4f4fdef6ae7c302a7b7720ef332fec0a8 arialb32.exe
  9c6df3feefde26d4e41d4a4fe5db2a89f9123a772594d7f59afd062625cd204e comic32.exe
  bb511d861655dde879ae552eb86b134d6fae67cb58502e6ff73ec5d9151f3384 courie32.exe
  2c2c7dcda6606ea5cf08918fb7cd3f3359e9e84338dc690013f20cd42e930301 georgi32.exe
  6061ef3b7401d9642f5dfdb5f2b376aa14663f6275e60a51207ad4facf2fccfb impact32.exe
  db56595ec6ef5d3de5c24994f001f03b2a13e37cee27bc25c58f6f43e8f807ab times32.exe
  5a690d9bb8510be1b8b4fe49f1f2319651fe51bbe54775ddddd8ef0bd07fdac9 trebuc32.exe
  c1cb61255e363166794e47664e2f21af8e3a26cb6346eb8d2ae2fa85dd5aad96 verdan32.exe
  64595b5abc1080fba8610c5c34fab5863408e806aafe84653ca8575bed17d75a webdin32.exe
...
