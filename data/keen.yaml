---
longname: "Commander Keen"
franchise: "Commander Keen"
genre: Platformer
# ITP: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=737587
engine: commander-genius

# https://keenwiki.shikadi.net/wiki/Main_Page

packages:
  ## Invasion of the Vorticons ##
  keen1-data:
    gog:
      url: commander_keen_complete_pack
    longname: "Commander Keen: Marooned on Mars"
    copyright: "\u00a9 1990 Id Software, Inc."
    install:
    - keen 1 assets

  keen2-data:
    gog:
      url: commander_keen_complete_pack
    longname: "Commander Keen: The Earth Explodes"
    copyright: "\u00a9 1991 Id Software, Inc."
    install:
    - keen 2 assets

  keen3-data:
    gog:
      url: commander_keen_complete_pack
    longname: "Commander Keen: Keen Must Die!"
    copyright: "\u00a9 1991 Id Software, Inc."
    install:
    - keen 3 assets
    # GOG has different versions of these files so they are split out
    - egalatch.ck3
    - level10.ck3
    - level13.ck3

  ## Lost episode ##
  keen-dreams-data:
    longname: "Commander Keen: Keen Dreams"
    copyright: "\u00a9 1991 Id Software, Inc."
    engine: ReflectionHLE
    install:
    - keen dreams assets

  ## Goodbye Galaxy ##
  keen4-data:
    gog:
      url: commander_keen_complete_pack
    longname: "Commander Keen: Secret of the Oracle"
    copyright: "\u00a9 1992 Id Software, Inc."
    install:
    - audio.ck4
    - egagraph.ck4
    - gamemaps.ck4

  keen5-data:
    gog:
      url: commander_keen_complete_pack
    longname: "Commander Keen: The Armageddon Machine"
    copyright: "\u00a9 1992 Id Software, Inc."
    install:
    - audio.ck5
    - egagraph.ck5
    - gamemaps.ck5

  keen6-data:
    longname: "Commander Keen: Aliens Ate My Babysitter"
    copyright: "\u00a9 1991 FormGen, Inc."
    install:
    - audio.ck6
    - cgagraph.ck6
    - egagraph.ck6
    - gamemaps.ck6

files:
  1keen.zip:
    download: ftp://ftp.3drealms.com/share/1keen.zip
    unpack:
      format: zip
    provides:
    - CK1SW131.SHR

  CK1SW131.SHR:
    unpack:
      format: id-shr-extract
    provides:
    - keen 1 assets

  setup_commander_keen_1.4_(42493).exe:
    unpack:
      format: innoextract
    provides:
    - keen 1 assets
    - keen 2 assets
    - keen 3 assets
    - egalatch.ck3?gog
    - level10.ck3?gog
    - level13.ck3?gog
    - audio.ck4
    - egagraph.ck4?1.43d
    - gamemaps.ck4
    - audio.ck5
    - egagraph.ck5?1.43d
    - gamemaps.ck5?1.43d

  # egalatch.ck3 from GOG is different
  egalatch.ck3:
    alternatives:
    - egalatch.ck3?orig
    - egalatch.ck3?gog

  # level10.ck3 from GOG is different
  level10.ck3:
    alternatives:
    - level10.ck3?orig
    - level10.ck3?gog

  # level13.ck3 from GOG is different
  level13.ck3:
    alternatives:
    - level13.ck3?orig
    - level13.ck3?gog

# Omnispeak only works with the 3D Realms version, so we prefer that
# (order is significant here)
  egagraph.ck4:
    alternatives:
    - egagraph.ck4?1.43d
    - egagraph.ck4?1.4gt

  egagraph.ck5:
    alternatives:
    - egagraph.ck5?1.43d
    - egagraph.ck5?1.4id
  gamemaps.ck5:
    alternatives:
    - gamemaps.ck5?1.43d
    - egagraph.ck5?1.4id

# Commander Genius only supports version 1.4 of Keen 6
# Omnispeak only supports versions 1.4 and 1.5 of Keen 6
  egagraph.ck6:
    alternatives:
    - egagraph.ck6?1.4
    - egagraph.ck6?1.5
  gamemaps.ck6:
    alternatives:
    - gamemaps.ck6?1.4
    - gamemaps.ck6?1.5

# this file is smaller than CK4SW14.SHR from 4keen.zip,
# so GDP will always try to download this one first
  4keen14.zip:
    download: http://spatang.com/files/4keen14.zip
    unpack:
      format: zip
    provides:
    - audio.ck4
    - egagraph.ck4?1.43d
    - gamemaps.ck4

  4keen.zip:
    download: ftp://ftp.3drealms.com/share/4keen.zip
    unpack:
      format: zip
    provides:
    # INSTALL.EXE
    # FILE_ID.DIZ
    - CK4SW14.SHR

  CK4SW14.SHR:
    unpack:
      format: id-shr-extract
    provides:
    - audio.ck4
    - egagraph.ck4?1.43d
    - gamemaps.ck4

# Data below covers:
# - Commander Keen 4, version 1.4 (EGA only, from 3D Realms and
#   Goodtimes Software as included in the Id Anthology)
# - Commander Keen 5, version 1.4 (EGA only, from 3D Realms and
#   the Id Anthology)
# - Commander Keen 6, versions 1.0, 1.4 and 1.5 (CGA and EGA)

groups:
  archives: |
      244484    7375d0452276388d52c35d0b3ad6ab82 1keen.zip
      178469    bb9316afc0c41f3703509ed24a2c0544 CK1SW131.SHR
      592887    1470ba6a18df1b315380b194b28a7a27 4keen14.zip
      729683    29cf97c6e636cf167ba22d0b12af9d43 4keen.zip
      659074    49b3565e48c9458e60927053e8cc8c6c CK4SW14.SHR

  gog archives: |
      14465576  7aa427c367531ab3e2fcdd3c50ad6495 setup_commander_keen_1.4_(42493).exe

  keen 1 assets: |
      15568     b1e632ee0483b66c2f1dff487547753b egahead.ck1
      57065     80a5870d9b48970333837fa0293561ed egalatch.ck1
      17633     ed9a0d1736034c155555543263939fe7 egasprit.ck1
      942       3d9b5805e21a7cc7b248664b0b8f6494 endtext.ck1
      8565      a44bcb798f931dc392903dcc5446c068 finale.ck1
      1859      476301a5a6ac41e39ea18d900e1c8e02 helptext.ck1
      1368      075a98e05b6d5bbed80ec21420577582 level01.ck1
      724       2d6b634fbce4aa357375d62300fb3e07 level02.ck1
      3474      956f2bbf2ad4fe7c52ad90962bd92166 level03.ck1
      1720      57b24d48be8748362a0c5e2277d2d859 level04.ck1
      810       1b6cd04bffb03fe26b722260da3316f9 level05.ck1
      928       7859497ced955aa87b4a935b43ae15b8 level06.ck1
      5650      337259268ca2338ee4d16818cbcd59d4 level07.ck1
      3416      51b099927527e88f8450163d05c2ca39 level08.ck1
      1638      de0e0de6b46474e2f4f06fc9abfd3e61 level09.ck1
      2086      0bb9ac865cdcbd46bdcacd493669d499 level10.ck1
      1636      b341dd49e84d7580a9eb4280827a7251 level11.ck1
      2178      edf1eb3db5a424360996e82ecf1dba93 level12.ck1
      9908      0cb4b321cdbb2d69682c258788ce8e4b level13.ck1
      7024      5d6d6814d7e7bf91dde2ec1792d45976 level14.ck1
      2234      09b0dc88bdb234d6b2efd5b8ac973dc0 level15.ck1
      5818      6ee167a36e7517320ff7ae6138f22e9d level16.ck1
      5638      bfe6781e44056cbc248701ae192122ac level80.ck1
      758       b86086e33b4fb169f96abdf002fd64e3 level81.ck1
      1262      dd1a09d6e332e0ae13c78670763ed891 level90.ck1
      27886     c8c4b2ce09a1d19d2763493c7e7ad44a preview2.ck1
      25429     eaff03166eba1d1d5da749b0d687b3a6 preview3.ck1
      2160      c480310fb66b36854b293264dbf6d8ff previews.ck1
      #204       26490f13ba856e0383bddc3c30c49db6 scores.ck1
      8898      c726f4948d49c5b212cc6f6bc8a72247 sounds.ck1
      3504      0f6e2c7fc0ec2df428757386b072b071 storytxt.ck1

  keen 2 assets: |
      17488     0311f19326f87f4bcd95472d3a5f8e19 egahead.ck2
      129856    11369fa63d5219ec9acba66f405a8781 egalatch.ck2
      35120     2247c907e819be12fcb510d4d48b0a28 egasprit.ck2
      12060     0b409a80da8b8883f48c7742014d62f9 finale.ck2
      1248      6f5537332f39059d0e52e912a7e74a41 level01.ck2
      2334      b9ed4769f2a61d13885695d24f4cf970 level02.ck2
      2316      be0263f823071224055c42b3a5c12345 level03.ck2
      3430      bab08532f4ba08aada35474638d7d419 level04.ck2
      2618      1cf3b6bce9c81b035d0258143d91e9a4 level05.ck2
      2936      403329b71eeeb9c4779228c7016e4a93 level06.ck2
      2636      20448c54ba554785fd479263055a8774 level07.ck2
      3852      62089b7d0bb4cc42cb8a0d8cdd6a32f4 level08.ck2
      3352      b6b5d0d1c839ff7c2f4bd7fea5177e6f level09.ck2
      4352      a3bb533ac0081d5af7f303de3f9ace2a level10.ck2
      2792      c0a33cd02ad8d04e9585d5b7973df61c level11.ck2
      3882      9776199572df8db0e9d69157c9e20118 level12.ck2
      2504      2544ec5775dedfa8aa759ceec7ea9970 level13.ck2
      2182      c202b8ee85e53b8c075f9f0e45bee6c7 level14.ck2
      3620      00e702807a84301b080df1523262b7a6 level15.ck2
      4318      14c93e7541131a9e7d1e6fb4f57c6ecf level16.ck2
      5460      a84553d13129d9dad84e04044c83e83d level80.ck2
      742       d314118e316d393060e9fa4906b8026b level81.ck2
      1334      88c826e6c27b2762e482b2c732eade38 level90.ck2
      #204       6d38d02b56b9d17d677802a7c4849c20 scores.ck2

  keen 3 assets: |
      19216     3f7c08d71323b77707e0a771dcee3eaa egahead.ck3
      43840     815739653172d23c3fc839e80c111126 egasprit.ck3
      18480     47ec73f0d727e756c9158b988a3f9a08 finale.ck3
      1830      c5ea5d1e9797c14182afbc18aba63ba8 level01.ck3
      2366      a85b95783ad529fca87d07a7dcf7a23e level02.ck3
      2572      8cc6b40384d9c31b280a2aa0f025f958 level03.ck3
      5978      76c92ee97b119c416fd692e8fe40a00c level04.ck3
      5252      95ccd6db3c44e6758145136eb8138299 level05.ck3
      1226      ffb27e921840f672ad1b4af19eda1c1d level06.ck3
      3074      b7f4590036b56ad6fbeeba1ecb520651 level07.ck3
      7226      e1a736a399c001b271d894ef1b380aae level08.ck3
      9288      40ece39678d0f0379bee8f2f6529b0ae level09.ck3
      1624      9b8429fd7aea5a458f2bd45339a87fb7 level11.ck3
      3048      0c5217fda750948f21b940a537acccf2 level12.ck3
      3130      1da7d7388b4e284fd71c16b229c8b689 level14.ck3
      11018     8a0d0903810b5a3fc704f6bfbf86b2a8 level15.ck3
      944       dd7716d8f6ff4f0f44d2a923fc7278f3 level16.ck3
      6054      6082480ce083fe86663fea3753feea5c level80.ck3
      320       11af6e2eeeeffe639fe48707c4757aba level81.ck3
      1424      9e2ef055188ec4fbee75a9ead1cfc1c9 level90.ck3

  keen dreams assets: |
      3498      6b1fa2e08b4627f50b756a9df8e3e90e audio.kdr
      213045    820efd208529f3b6950fa5fb551da49f egagraph.kdr
      63497     a3f1b0a5b3f988725bfface3dcabc2f8 gamemaps.kdr

size_and_md5: |
  33325     125c93a549a3e5b2ab4c6c6ec1ad3e7d audio.ck4
  101990    d080b7c53b0b374813a7479b559d4eb4 audio.ck5
  56265     8078f8106cca87fe7bea6b0dca798e5b audio.ck6
  311897    1e7269dc584db84bddda604a6bbbfa54 cgagraph.ck6
  520581    e38064818169e365bcff61dbaec55aef egagraph.ck4?1.43d
  513074    d4d05966870a3a8bd47e431b138119a4 egagraph.ck4?1.4gt
  573664    d5598dcda25b02e61aeac17fcac9437b egagraph.ck5?1.43d
  572916    f57c6fb5b066f78cd1c25f43e6277f96 egagraph.ck5?1.4id
  457309    81cd87b78405655a66bdbfed7862585a egagraph.ck6?1.0
  464726    c8d4f8575f781ecde2edb71a86c91c20 egagraph.ck6?1.4
  464662    a99dd1cee0830a67b125c7b68f28b039 egagraph.ck6?1.5
  99040     9e1811deb429f7edc6bffa5bb786ebb4 gamemaps.ck4
  73721     33ffac175c3fb8e33fc3dbdc0ab6b965 gamemaps.ck5?1.43d
  73736     86edb0e08ce8bd4696d5581a9a646a14 gamemaps.ck5?1.4id
  95421     99e5514817e46f619d62237052679757 gamemaps.ck6?1.0
  95541     161bb004fb820cd1dea2674af234c249 gamemaps.ck6?1.4
  95528     0002d6a9f5a23ba3427acb0337efb2f8 gamemaps.ck6?1.5
  116800    075d605deb211396e55b84dc8f774dd7 egalatch.ck3?orig
  116800    de588c164eae25403a144e2b6f53b4b7 egalatch.ck3?gog
  2736      f06342986c7643f31e53328f9b8bcdd1 level10.ck3?orig
  2736      e7e63cecf3e04600b359181e66b8a5e2 level10.ck3?gog
  6172      ec34832741ef08ee787880d69748f07c level13.ck3?orig
  6172      2537f8cec4914b40515e2deb70e82b9b level13.ck3?gog

sha1sums: |
  0edd40a10a85fae75d7f4ba82f17a062d8e741bf 1keen.zip
  85fd7115d81888c4ed44d79f00ee58d9e7c38fd0 CK1SW131.SHR
  bde2a69ad5ea36601dcfeb6dbfe2232fcc486161 4keen14.zip
  401b3ee98f633043ebd8bc7fbe0c397e60cc0558 4keen.zip
  e16308782c4fbf965bdb7d81d67a856f755e7aeb CK4SW14.SHR
  66baf85b886b3b1f1882b40d0e8061af19187453 audio.ck4
  5ded3b6afde6e102b9ea7e39887fc94899eb3476 audio.ck5
  d83153a4b5caea106fb3eb729edbe2271955648f audio.ck6
  d1b35a0988b72486ae73bab5daf8e797af49f927 cgagraph.ck6
  cf3925b037af8e9f67d10206189261fdc335629f egagraph.ck4?1.43d
  7ad344cc19e0a3e85ae1195133439aa9f76e3b81 egagraph.ck4?1.4gt
  60226757446de430635fce8b924fb5b70d8492ca egagraph.ck5?1.43d
  6f23e0af005fbb1406154caf0c522f9e49cbd94b egagraph.ck5?1.4id
  bb66bde6a909355328bdc4b4a5ea8e4efeaeec4b egagraph.ck6?1.0
  75a723fd66846dc2e0e177a602d8944927328686 egagraph.ck6?1.4
  5c0d381f0ee21a22a9d555c1f185ccc3f4de7978 egagraph.ck6?1.5
  953da626d8a81fb890e079287d5033f10bdc6b7d gamemaps.ck4
  d8c55fee10b3fdc4a54ce2778ad4b21b99ce391b gamemaps.ck5?1.43d
  61f16a83cecbb4008b0f8fe550896f2b3ee385ca gamemaps.ck5?1.4id
  62aad31e5cf91518e269e5d0d513bf178e0c6ae9 gamemaps.ck6?1.0
  14b054d4c0752aba4f7651998b85f446114bd235 gamemaps.ck6?1.4
  27f9dd03e99176eede3ccf65a06260a5d91da96b gamemaps.ck6?1.5
  bf9ceef2215d7fe8269724ab6114b182d273b7ae setup_commander_keen_1.4_(42493).exe

sha256sums: |
  2fa260d48a1c7710c819e89594fff727ba6bc3e42065b5d8cedeccea256c2983 4keen14.zip
  f7c63feb8bbec817646d95aa26313df1654c01eda8f7ccc5cfd0c0bc0b5e305e 4keen.zip
  1828b1db5e44bbefa7b1f31595272dd53067165a0ec86d0179f8558a6049a2e5 CK4SW14.SHR
  66dcf6070a4bc5c8933815b15dab7145ddcccb959f8f43af2a0449b085a04366 audio.ck4
  1a9aae3de1d43d02334e1ad3b5063b64fd71766aed5e574431d7b3fdd3bfd02f audio.ck5
  5eef576c62773bc7c54c2c55c56117dd89ae7aba1d826f1bd201a8699d07b0e3 audio.ck6
  1a0995bfb8ebd3ddc0f774969c7fc59b33a3f73f235119bdb47ac0846458b1fd cgagraph.ck6
  9805168fb910385ad40455541cbcfa1aaa61f9d81b2504274ee0ca0ec4b71550 egagraph.ck4?1.43d
  d228fff333994921a34c137807246597073ee457202e6fab7daa2807ed60a7b0 egagraph.ck4?1.4gt
  fa90d780c32d262e812e552a7bda9de33fe9397d351246c3c10a4eebdd9973dc egagraph.ck5?1.43d
  4070e4962a1a078cbe9f5d319c1861bcec45a9b13654910b3a73ee97d4c4c484 egagraph.ck5?1.4id
  84d782e776dcf0e431231cce4ca8aebf10bbb5c6ae6fd25670fe28a0dce0b675 egagraph.ck6?1.0
  91892b7ef21f7a647601cb27a07895f3aad536d3eb404a1bdbadcbbea8b9a305 egagraph.ck6?1.4
  b34edd96735f2bc8a63361ba6a83f2c484d29e57dca2f36be10232d02c0db7aa egagraph.ck6?1.5
  01186f8cb99257b69a4f475186deb9ba79b872ed52378131c79c21f6344753ea gamemaps.ck4
  0f0ae9bb64b2d511b7fbda95c67d069412643f45d6935cee060d5ba2a5dce616 gamemaps.ck5?1.43d
  4f2a88b1cfad55e424eab5cd92fbadcef0b574557113ae1ed41a2fb216438697 gamemaps.ck5?1.4id
  48047f19d0f7a69db5f351f342868d52a5101e1732129ba297acb35439f8b50e gamemaps.ck6?1.0
  518e4179039e307430c419386ba5fc55ff49b2844aeec27de8eb4b1e9409b3b8 gamemaps.ck6?1.4
  50dfc40a055076c3d4aca3d8335e7291759f1e8416bd2086d0cff2a52a90d8d3 gamemaps.ck6?1.5
  1c5322e3bc9e5c6be1295e981a310e47b1113d987e6be2720fe975c149b4cf41 setup_commander_keen_1.4_(42493).exe

...
